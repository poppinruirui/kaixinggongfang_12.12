class CTiaoZiManager extends CObj {

    protected static s_lstRecycledTiaoZi:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static s_lstcontainerTiaoZi:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static s_fAlphaChangeSpeed:number = 0;
    public static s_fPosChangeA:number = 0;
    public static s_fPosChangeV0:number = 0;

    public static Init():void
    {
      CTiaoZiManager.s_fAlphaChangeSpeed = -1 / (CDef.s_fTiaoZiTotalTime * 0.5 );
      CTiaoZiManager.s_fPosChangeA = CyberTreeMath.GetA( CDef.s_fTiaoZiTotalDistance, CDef.s_fTiaoZiTotalTime );
      CTiaoZiManager.s_fPosChangeV0 = CyberTreeMath.GetV0( CDef.s_fTiaoZiTotalDistance, CDef.s_fTiaoZiTotalTime );
    }

    public static NewTiaoZi():CTiaoZi
    {
        var tiaozi:CTiaoZi = null;
        if (  CTiaoZiManager.s_lstRecycledTiaoZi.numChildren > 0 )
        {
            tiaozi = CTiaoZiManager.s_lstRecycledTiaoZi.getChildAt(0) as CTiaoZi;
            CTiaoZiManager.s_lstRecycledTiaoZi.removeChildAt(0);
            tiaozi.Reset();
        }
        else
        {
            tiaozi = new CTiaoZi();
            tiaozi.anchorOffsetX = tiaozi.width / 2;
            tiaozi.anchorOffsetY = tiaozi.height;
        }
        
        CTiaoZiManager.s_lstcontainerTiaoZi.addChild( tiaozi );

        return tiaozi;
    }

    public static DeleteTiaoZi( tiaozi:CTiaoZi ):void
    {
        CTiaoZiManager.s_lstRecycledTiaoZi.addChild( tiaozi );
    }

    public static FixedUpdate():void
    {
        for ( var i:number = CTiaoZiManager.s_lstcontainerTiaoZi.numChildren - 1; i >= 0; i-- )
        {
            var tiaozi:CTiaoZi = CTiaoZiManager.s_lstcontainerTiaoZi.getChildAt(i) as CTiaoZi;
            tiaozi.FixedUpdate();
        }
    }

} // end class