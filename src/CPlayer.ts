
class CPlayer extends egret.DisplayObjectContainer {

    /*
    注意，钻石是绑定玩家账号的，不会清空。即便城市重生了，钻石也不会消失
    */
     protected static s_nDiaomonds:number = 10000000; // 当前钻石数量

     // 注：DoubleTime状态是绑在Player身上的，即便城市切换到下一级或者重生了，也不会清零s
     protected static s_nDoubleTimeLeft:number = 0;
     protected static s_bDoubleTiming:boolean = false;

     protected static m_dicCityHallGamePageAffect:Object = new Object();

     public constructor() {
        super();


     } // end constructor

     public static  GetDiamond():number
     {
         return CPlayer.s_nDiaomonds;
     }

     public static  SetDiamond( nDiamond:number ):void
     {
          CPlayer.s_nDiaomonds = nDiamond;

          CUIManager.s_uiMainTitle.SetDiamond( nDiamond );
     }

     public static BeginDoubleTime( nDoubleTime:number ):void
     {
         CPlayer.s_nDoubleTimeLeft = nDoubleTime;
         CPlayer.s_bDoubleTiming = true;

         Main.s_CurTown.UpdateCPS();

          CAutomobileManager.ChangeCarSpeedDueToDoubleTime(4);
         CUIManager.s_uiCPosButton.SetDoubleTimeLeftVisible( true );


          var music_url:string = CResourceManager.url_root + "Audio/accelerate.mp3";
                  RES.getResByUrl( music_url ,CPlayer.onLoadAudioComplete,this,RES.ResourceItem.TYPE_SOUND); 
     }

  protected static m_Sound:egret.Sound;
      protected static  onLoadAudioComplete(event:any):void {
        
          this.m_Sound = <egret.Sound>event;
        this.m_Sound .play( 0, 1 );
   

         

      }


     // 一秒一次的轮询
     public static MainLoop_1():void
     {
         CPlayer.DoubleTimeLoop();
     }

     protected static DoubleTimeLoop():void
     {
         if ( !CPlayer.s_bDoubleTiming )
         {
             return;
         }

         CPlayer.s_nDoubleTimeLeft -= 1; // 单位是秒
         var nMin:number = Math.floor( CPlayer.s_nDoubleTimeLeft / 60 );
         var szMin = nMin.toString();
         if ( nMin == 0 )
         {
             szMin = "<1";
         }
         CUIManager.s_uiCPosButton.SetDoubleTimeLeft( szMin );

         if ( CPlayer.s_nDoubleTimeLeft <= 0 )
         {
             CPlayer.EndDoubleTime();
         }

        
     }

     public static IsDoubleTime():boolean
     {
         return CPlayer.s_bDoubleTiming;
     }

     protected static EndDoubleTime():void
     {
          CPlayer.s_bDoubleTiming = false;
          CPlayer.s_nDoubleTimeLeft  = 0;

           CAutomobileManager.ChangeCarSpeedDueToDoubleTime(0.25);
        CUIManager.s_uiCPosButton.SetDoubleTimeLeftVisible( false );
     }

     public static GetMoney( eMoneyType:Global.eMoneyType ):number
     {
         if ( eMoneyType == Global.eMoneyType.coin )
         {
             return Main.s_CurTown.GetCoins();
         }
         else if ( eMoneyType == Global.eMoneyType.diamond )
         {
             return CPlayer.GetDiamond();
         }
         return -1;
     }

     public static SetMoney( eMoneyType:Global.eMoneyType, nValue:number ):void
     {
         if ( eMoneyType == Global.eMoneyType.coin )
         {
              Main.s_CurTown.SetCoins(nValue, true);
         }
         else if ( eMoneyType == Global.eMoneyType.diamond )
         {
              CPlayer.SetDiamond(nValue);
         }

     }

     public static SetCityHallGamePageAffect( nIndex:number, nValue:number ):void
     {
         CPlayer.m_dicCityHallGamePageAffect[nIndex] = nValue;
     }

     public static GetCityHallGamePageAffect( nIndex:number ):number
     {
         return CPlayer.m_dicCityHallGamePageAffect[nIndex];
     }

} // end class