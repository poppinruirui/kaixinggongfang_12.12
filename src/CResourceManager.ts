class CResourceManager extends egret.DisplayObjectContainer{
    
    public static url:string = "http://39.108.53.61:5566/resource/assets/Spr/" ;
    public static url_root:string = "http://39.108.53.61:5566/resource/assets/";


    public static url_local:string = "/resource/assets/" ;  



      public static s_containerRecycledConstructingAni:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();

    public static NewConstructingAni():CConstructingAnimation
    {
        var ani:CConstructingAnimation = null;

        if ( CResourceManager.s_containerRecycledConstructingAni.numChildren > 0 )
        {
            ani = CResourceManager.s_containerRecycledConstructingAni.getChildAt(0) as CConstructingAnimation;
            CResourceManager.s_containerRecycledConstructingAni.removeChildAt(0);
            ani.visible = true;
        }
        else
        {
            ani = new CConstructingAnimation();
        }

        return ani;
    }

    public static DeleteConstructingAni( ani:CConstructingAnimation ):void
    {
        ani.visible = false;
        CResourceManager.s_containerRecycledConstructingAni.addChild( ani );
    }


    public static FixedUpdate():void
    {


    }

    public static Init():void
    {
       CResourceManager.s_bmpSegTop = new egret.Bitmap( RES.getRes( "Hurdle_Top_png" ) );
       CResourceManager.s_bmpSeg = new egret.Bitmap( RES.getRes( "Hurdle_Seg_png" ) );
    }

   public static s_bmpSegTop:egret.Bitmap = null;
   public static s_bmpSeg:egret.Bitmap = null; 

   protected static s_containerRecycledProgressBar:eui.Component = new eui.Component();
   public static NewProgressBar():CUIBaseProgressBar
   {
       var progressBar:CUIBaseProgressBar = null;

       if ( CResourceManager.s_containerRecycledProgressBar.numChildren > 0 )
       {
           progressBar = CResourceManager.s_containerRecycledProgressBar.getChildAt(0) as CUIBaseProgressBar;
           CResourceManager.s_containerRecycledProgressBar.removeChildAt(0);
           progressBar.Loop(0);
       }
       else
       {
           progressBar = new CUIBaseProgressBar( "resource/assets/MyExml/CProgressBar.exml" );
       }

       return progressBar;
   }

   public static DeleteProgressBar(progressBar:CUIBaseProgressBar):void
   {
        CResourceManager.s_containerRecycledProgressBar.addChild(progressBar);
   }

   static s_containerRecycledDiBiao:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
   public static NewDiBiao():CObj
   {
       var dibiao:CObj = null;
       if ( CResourceManager.s_containerRecycledDiBiao.numChildren > 0 )
       {
           dibiao = CResourceManager.s_containerRecycledDiBiao.getChildAt(0) as CObj;
           CResourceManager.s_containerRecycledDiBiao.removeChildAt(0);
       }
       else
       {
           dibiao = new CObj();
       }

       return dibiao;
   }

   public static DeleteDiBiao( dibiao:CObj ):void
   {
       CResourceManager.s_containerRecycledDiBiao.addChild( dibiao );
   }

/////////////////////////
    static s_nTotalObj:number = 0;
    protected static s_lstRecycledObjs:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static  NewObj():CObj
    {
        return new CObj();

        var obj:CObj = null;
        if ( CResourceManager.s_lstRecycledObjs.numChildren > 0 )
        {
            obj = CResourceManager.s_lstRecycledObjs.getChildAt(0) as CObj;
            CResourceManager.s_lstRecycledObjs.removeChildAt(0);
            obj.Reset();
        }
        else
        {
            obj = new CObj();
            CResourceManager.s_nTotalObj++;
            
        }

        return obj;
    }

    public static DeleteObj( obj:CObj ):void
    {
        CResourceManager.s_lstRecycledObjs.addChild( obj );
    }

    public static GetDiBiaoTextureByTownId( nTownId:number ):egret.Texture
    {
        var szResName:string = "dibiao" + nTownId + "_png";
       // console.log( "moutain res name=" + szResName );
       return RES.getRes( szResName );

        
    }

    public static GetTreeTextureByTownId(nTownId:number):egret.Texture
    {
        return RES.getRes( "tree" + nTownId + "_png" );
    }
    
    public static GetMoutainTextureByTownId(nTownId:number):egret.Texture
    {
        return RES.getRes( "moutain" + nTownId + "_png" );
    }

    public static GetBubbleTextureByTownId(nTownId:number ):egret.Texture
    {
        return RES.getRes( "bubble" + nTownId + "_png" );
    }

    public static GetCloudResNameByTownId( nTownId:number, nCloudSubId:number = 0  ):string
    {
       return CResourceManager.url + "Spr/TownsRes/" + nTownId + "/cloud.png";
    }

  

    public static GetCloudResNameByTownId_Local( nTownId:number, nCloudSubId:number = 0 ):string
    {
        if ( nCloudSubId > 0 )
        {
             return "cloud" + nTownId + "_" + nCloudSubId + "_png";
        }
        return "cloud" + nTownId + "_png";
    }

    public static GetCloudTextureByTownId(nTownId:number, nCloudSubId:number = 0 ):egret.Texture
    {
         if ( nCloudSubId == 0 )
         {   
            return RES.getRes( "cloud" + nTownId + "_png" );

          
         }
         else
         {
             return RES.getRes( "cloud" + nTownId + "_" +  nCloudSubId +"_png" );
         }
    }

    public static GetNumberTexture( nTownId:number ):egret.Texture
    {
        return RES.getRes( nTownId + "_png" );
    }

    ///// 
    protected static s_lstRecycledVectors:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static NewVector():CVector
    {
        var vec:CVector = null;
        if ( CResourceManager.s_lstRecycledVectors.numChildren > 0 )
        {
            vec = CResourceManager.s_lstRecycledVectors.getChildAt(0) as CVector;
            CResourceManager.s_lstRecycledVectors.removeChildAt(0);
        }
        else
        {   
            vec = new CVector();
        }

        return vec;
    }

    public static DeleteVector( vec:CVector ):void
    {
        CResourceManager.s_lstRecycledVectors.addChild( vec );
    }


    /////
    protected static s_lstRecycledShapes:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public NewShape():egret.Shape
    {
        var shape:egret.Shape = null;
        if ( CResourceManager.s_lstRecycledShapes.numChildren > 0 )
        {
            shape =   CResourceManager.s_lstRecycledShapes.getChildAt(0) as egret.Shape;
            CResourceManager.s_lstRecycledShapes.removeChildAt(0);
        }
        else
        {   
            shape = new egret.Shape();
        }

        return shape;
    }

    public static  DeleteShape( shape:egret.Shape ):void
    {
        CResourceManager.s_lstRecycledShapes.addChild( shape ); 
    }

    protected static s_lstRecycledDistricts:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static DeleteDistrict( district:CDistrict ):void
    {
        CResourceManager.s_lstRecycledDistricts.addChild( district );
    }

    public static NewDistrict():CDistrict
    {
        var district:CDistrict = null;
        if ( CResourceManager.s_lstRecycledDistricts.numChildren > 0 )
        {
            district = CResourceManager.s_lstRecycledDistricts.getChildAt(0) as CDistrict;
            CResourceManager.s_lstRecycledDistricts.removeChildAt(0);
            district.ResetAll();
        }
        else
        {
            district = new CDistrict();
        }
        return district;
    }

    protected static s_lstRecycledDistrictSketchs:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static NewDistrictSketch():CUIDistrictSketch
    {
        var sketch:CUIDistrictSketch = null;
        if ( CResourceManager.s_lstRecycledDistrictSketchs.numChildren > 0 )
        {
            sketch =  CResourceManager.s_lstRecycledDistrictSketchs.getChildAt(0) as CUIDistrictSketch;
            CResourceManager.s_lstRecycledDistrictSketchs.removeChildAt(0);
        }
        else
        {
            sketch = new CUIDistrictSketch();
        }
        return sketch;
    }

    public static DeleteDistrictSketch(sketch:CUIDistrictSketch):void
    {
        CResourceManager.s_lstRecycledDistrictSketchs.addChild( sketch );
    }

    /////////
    static s_lstRecycledWindTurbine:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static  DeleteWindTurbine( obj:CWindTurbine ):void
    {
        CResourceManager.s_lstRecycledWindTurbine.addChild( obj );
    }


    //// 
    static s_lstRecyceFlyJinBi:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static NewFlyJinBi():CObj
    {
        return new CObj;
    }

    public static DeleteFlyJinBi( obj:CObj ):void
    {
        CResourceManager.s_lstRecyceFlyJinBi.addChild( obj );
    }

} // end class