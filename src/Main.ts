//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the 
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////


    


class Main extends eui.UILayer {


    public static SCENE_WIDTH:number = 640;
    public static SCENE_HEIGHT:number = 1136;

    protected createChildren(): void {
        super.createChildren();

        egret.lifecycle.addLifecycleListener((context) => {
            // custom lifecycle plugin
        })

        egret.lifecycle.onPause = () => {
            egret.ticker.pause();
        }

        egret.lifecycle.onResume = () => {
            egret.ticker.resume();
        }

        //inject the custom material parser
        //注入自定义的素材解析器
        let assetAdapter = new AssetAdapter();
        egret.registerImplementation("eui.IAssetAdapter", assetAdapter);
        egret.registerImplementation("eui.IThemeAdapter", new ThemeAdapter());


        this.runGame().catch(e => {
            console.log(e);
        })
    }

    private async runGame() {
        await this.loadResource()
        this.createGameScene();
        const result = await RES.getResAsync("description_json")
     //   this.startAnimation(result);
        await platform.login();
        const userInfo = await platform.getUserInfo();
       // console.log(userInfo);

    }

    private async loadResource() {
        try {
          //  const loadingView = new LoadingUI();
            //this.stage.addChild(loadingView);
            await RES.loadConfig("resource/default.res.json", "resource/");
            await this.loadTheme();
            await RES.loadGroup("preload", 0, null);
           // this.stage.removeChild(loadingView);

        }
        catch (e) {
            console.error(e);
        }

    }

    private loadTheme() {
        return new Promise((resolve, reject) => {
            // load skin theme configuration file, you can manually modify the file. And replace the default skin.
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            let theme = new eui.Theme("resource/default.thm.json", this.stage);
            theme.addEventListener(eui.UIEvent.COMPLETE, () => {
                resolve();
            }, this);

        })
    }

    private textfield: egret.TextField;
    /**
     * 创建场景界面
     * Create scene interface
     */
 protected m_containerUI:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
 protected m_listOperate:eui.List = new eui.List();
 public static s_panelMsgBox:CUIMsgBox;

 protected m_btnSave:eui.Button;
 protected m_btnZoomIn:eui.Button; // 放大
 protected m_btnZoomOut:eui.Button; // 缩小



protected CreateUI():egret.DisplayObjectContainer
{
   this.addChild( this.m_containerUI );

   
    
 
   


    Main.s_panelMsgBox = new CUIMsgBox();
    this.m_containerUI.addChild( Main.s_panelMsgBox);
    Main.s_panelMsgBox.x = 50;
    Main.s_panelMsgBox.y = 200;
    Main.s_panelMsgBox.visible = false;

    
        CUIManager.s_containerUIs = new egret.DisplayObjectContainer();
       // this.addChild( CUIManager.s_containerUIs );
        CUIManager.Init(  );



    return this.m_containerUI;
}




 
    public static s_CurTown:CTown;


    public static s_aryPreloadResName:Array<string> = new Array<string>();

    protected static s_aryMatchedResName:Array<string> = new Array<string>();
    public static SeekResByName( szContent:string ):Array<string>
    {
        Main.s_aryMatchedResName.length = 0;

        for ( var i:number = 0; i < Main.s_aryPreloadResName.length; i++ )
        {
            var szResName:string = Main.s_aryPreloadResName[i];
            if ( szResName.search( szContent ) != -1 )
            {
                Main.s_aryMatchedResName.push( szResName );
            }
        }
        return Main.s_aryMatchedResName;
    }

    public static s_dicTowns:Object = new Object();

    public static GetTownByName( szName:string ):CTown
    {
        return Main.s_dicTowns[szName];
    }

    protected m_timerMainLoop:egret.Timer = new egret.Timer( CDef.s_fFixedDeltaTime * 1000, 0 );
    protected m_timerMainLoop_1:egret.Timer = new egret.Timer( 1000, 0 );
    protected m_timerMainLoop_60:egret.Timer = new egret.Timer( 60000, 0 );
    protected m_timerPreHandleStageTap:egret.Timer = new egret.Timer( 100, 1 );
    
    // 一秒钟一次的轮询
    private FixedUpdate_1()
    {

        if ( Main.s_CurTown )
        {

             if ( Main.s_CurTown )
             {
                Main.s_CurTown.FixedUpdate_1();
            }

        
        }

          CPlayer.MainLoop_1();

            CUIManager.MainLoop_1();

        if ( Main.m_CloudManager )
       {
           Main.m_CloudManager.CloudLoop_1Sec();
       }


if ( CUIManager.s_uiMainTitle )     
            {
                CUIManager.s_uiMainTitle.ShowDebugInfo( CResourceManager.s_nTotalObj.toString() );
            }
    }

    // 一分钟一次的轮询
    private FixedUpdate_60()
    {
      

       
    }

    public static s_nTapOtherPlaceBeforeTapStage:boolean = false;
    protected FixedUpdate_PreHandleStageTap():void
    {
        if ( !Main.s_nTapOtherPlaceBeforeTapStage )
        {
            this.ReallyDo_HandleStageUp();
        }
        Main.s_nTapOtherPlaceBeforeTapStage = false;
        this.m_timerPreHandleStageTap.stop();
    }

    // 最小间隔的轮询，以CDef中的定义为准。目前是25毫秒
    // 注意，这种轮询一定要谨慎，不然可能有性能问题
    private FixedUpdate()
    {
  
       if ( Main.s_CurTown )
       {
           Main.s_CurTown.FixedUpdate();
       }

       CJinBiManager.FixedUpdate();
       CTiaoZiManager.FixedUpdate();

       CUIManager.MainLoop();

       if ( Main.m_CloudManager )
       {
           Main.m_CloudManager.CloudLoop();
       }
       

       CTapModule.FixedUpdate();
       
       CEffectManager.FixedUpdate();
    }


 protected m_bitmapTest:egret.Bitmap = null;
 
public m_aryTestXiShu:Array<number> = new Array<number>();
public m_aryTestData:Array<number> = [

0.46,
0.87,
1.48,
2.34,
3.50,
5.01,
6.93,
9.32 ,
12.24,
15.76,
19.93,
24.82,
30.50,
37.05,
44.53,
53.02,
62.59,
73.31,
85.28,
98.56,
113.23,
129.39,
147.10,
166.47,
187.56,
210.48,
235.31,
262.13,
291.04,
322.14,
355.50,
391.24,
429.44,
470.20,
513.61,
559.79,
608.81,
660.8,
715.84,
774.04
]
;


protected m_aryCha:Array<number> = 
[
    0.41,
0.61,
0.86,
1.16,
1.510000,
1.920000,
2.390000,
2.920000,
3.520000,
4.170000,
4.889999,
5.680000,
6.549999,
7.480000,
8.490002,
9.570000,
10.719997,
11.970001,
13.279999,
14.670006,
16.159996,
17.710007,
19.369995,
21.089996,
22.919998,
24.830002,
26.820007,
28.910004,
31.100006,
33.359985,
35.739990,
38.200012,
40.760010,
43.409973,
46.179993,
49.020020,
51.989990,
55.040039,
58.199951
];

protected m_aryCalculatedValues:Array<number> = new Array<number>();
protected m_aryCalcylatedChaZhi:Array<number> = new Array<number>();
public TestData():void
{
    console.log( "m_aryCha.len=" + this.m_aryCha.length );
    this.m_aryCalcylatedChaZhi[0] = 41000;
    for ( var i = 1; i < 40; i++ )
    {
        this.m_aryCalcylatedChaZhi[i] = Math.pow( i * 0.21, 1.9) ;
        console.log( "worinima:" + this.m_aryCalcylatedChaZhi[i]);
    }

   /*
    for ( var i:number = 1; i < this.m_aryTestData.length; i++ )
    {

        var xishu:number =  ( this.m_aryTestData[i] - this.m_aryTestData[i-1] ) ;
      
       this.m_aryTestXiShu.push(xishu)  
    }*/
   var containerTemp:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
   //this.addChild(containerTemp);
   
  

    var x_zhou:egret.Shape = new egret.Shape();
    var y_zhou:egret.Shape = new egret.Shape();
    containerTemp.addChild(x_zhou);
    containerTemp.addChild(y_zhou);
    x_zhou.graphics.lineStyle( 1, 0x00FF00 );
    y_zhou.graphics.lineStyle( 1, 0x00FF00 );
    x_zhou.graphics.moveTo(0,0);
     x_zhou.graphics.lineTo( 10000,  0 );
    y_zhou.graphics.moveTo(0,0);
     y_zhou.graphics.lineTo( 0,  10000 );
    
    var tubiao:egret.Shape = new egret.Shape();
    containerTemp.addChild( tubiao );
    containerTemp.x = 10;
    containerTemp.y = 800;
    tubiao.graphics.lineStyle( 1, 0xFF0000 );
    tubiao.graphics.moveTo( 0, 0 );
    var fangda:number = 10;
    containerTemp.scaleX= fangda;
    containerTemp.scaleY = -fangda;
     for ( var i:number = 0; i < this.m_aryCha.length; i++ )
    {
        var x:number = i;
        var y:number = this.m_aryCha[i];
       
         tubiao.graphics.lineTo( x, y );
    
    }


for ( var i:number = 1; i < 100; i+=5 )
  {
      var point:egret.Shape = new egret.Shape();
      point.graphics.beginFill( 0x00FF00 );
      point.graphics.drawCircle( i, 0, 0.5 );
      point.graphics.endFill();
      containerTemp.addChild( point );
     

      point = new egret.Shape();
      point.graphics.beginFill( 0x00FF00 );
      point.graphics.drawCircle( 0, i, 0.5 );
      point.graphics.endFill();
      containerTemp.addChild( point );

      
  }




}

public static m_CloudManager:CCloudManager = new CCloudManager();

    protected createGameScene(): void {



egret.ImageLoader.crossOrigin = "anonymous";
     //   console.log( this.stage.stageWidth + "_" + this.stage.stageHeight );
 

egret.ImageLoader.crossOrigin = "anonymous"; 


        CDataManager.LoadMapData();

        var stageBg:egret.Shape = new egret.Shape();
        this.addChild( stageBg );
       stageBg.touchEnabled = false;

       stageBg.visible = false;
        
        /*
        stageBg.graphics.beginFill( 0x4169E1, 1);
        stageBg.graphics.drawRect( 0, 0, this.stage.stageWidth, this.stage.stageHeight );
        stageBg.graphics.endFill();
        */
        // 绘制渐变色
  
       
        

        var testPanel:egret.Bitmap = new egret.Bitmap();
       // this.addChild( testPanel );
        testPanel.texture = RES.getRes( "BaseRect_png" );
        testPanel.width = this.stage.stageWidth;
        testPanel.height = this.stage.stageHeight;
        testPanel.touchEnabled = true;
      


        // end 绘制渐变色

        CSoundManager.Init();
        CJinBiManager.Init();

        CColorFucker.Init();


        this.m_timerMainLoop .addEventListener(egret.TimerEvent.TIMER,this.FixedUpdate,this);
        this.m_timerMainLoop.start();

        this.m_timerMainLoop_1.addEventListener(egret.TimerEvent.TIMER,this.FixedUpdate_1,this);
        this.m_timerMainLoop_1.start();

        this.m_timerMainLoop_60.addEventListener(egret.TimerEvent.TIMER,this.FixedUpdate_60,this);
        this.m_timerMainLoop_60.start();

        this.m_timerPreHandleStageTap.addEventListener(egret.TimerEvent.TIMER,this.FixedUpdate_PreHandleStageTap,this);

        var fDi:number =  Math.sqrt( CDef.s_nTileWidth * CDef.s_nTileWidth + CDef.s_nTileHeight * CDef.s_nTileHeight );
        var fCos:number = CDef.s_nTileWidth / fDi;
        var fSin:number = CDef.s_nTileHeight / fDi;
        
        CDef.s_fAutomobileMoveDirX = fCos * CDef.s_fAutomobileMoveSpeed;
        CDef.s_fAutomobileMoveDirY = fSin * CDef.s_fAutomobileMoveSpeed;

       // 配置文件管理器
       CConfigManager.Init();

       CTiaoZiManager.Init(); // 跳字管理器 初始化

       // 资源管理器，初始化
       CResourceManager.Init();

      var aryPreloadResources:RES.ResourceItem[] =  RES.getGroupByName("preload");
      for ( var i:number = 0; i < aryPreloadResources.length; i++ )
      {
          var item:RES.ResourceItem = aryPreloadResources[i];
          Main.s_aryPreloadResName.push( item.name );
      }

      
     
/*
        let button = new eui.Button();
        button.label = "Click!";
        button.horizontalCenter = 0;
        button.verticalCenter = 0;
        this.addChild(button);
        button.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick, this);
        */
        Main.m_CloudManager = new CCloudManager();
        Main.m_CloudManager.visible = false;

       // this.m_CloudManager.InitClouds(1, this.stage.stageWidth, this.stage.stageHeight);
        this.addChild( Main.m_CloudManager );
         var matrix:egret.Matrix = new egret.Matrix();
        matrix.createGradientBox( this.stage.stageHeight, this.stage.stageWidth);

         Main.m_CloudManager.SetStageBg( stageBg, matrix  );
  stageBg.visible = true;
       stageBg.graphics.beginGradientFill( egret.GradientType.LINEAR, [0x54c5f1, 0x81aee7], [1,1], [0,255],matrix );
       // stageBg.graphics.beginFill( 0x00FF00, 1 );
        stageBg.graphics.drawRect( 0, 0, this.stage.stageHeight, this.stage.stageWidth  );
        stageBg.graphics.endFill();
        stageBg.rotation = 90;
        stageBg.x = this.stage.stageWidth;
        stageBg.y = 0;

      // 创建ui
      var containerUI:egret.DisplayObjectContainer =  this.CreateUI();
      
         // 创建一个城镇
      Main.s_CurTown = new CTown( 1, this.stage.stageWidth, this.stage.stageHeight );
        Main.s_CurTown.visible = false;
   
      this.addChild( Main.s_CurTown );


     
      Main.s_CurTown.SetCloudManager(Main.m_CloudManager);
    
    Main.s_CurTown.ClearAll();
    Main.s_CurTown.LoadTownData(1);

    



   // Main.s_CurTown.addEventListener( egret.TouchEvent.TOUCH_BEGIN, this.handleTownDown, this );
  //  Main.s_CurTown.addEventListener( egret.TouchEvent.TOUCH_END, this.handleTownUp, this );
    //Main.s_CurTown.addEventListener( egret.TouchEvent.TOUCH_MOVE, this.handleTownMove, this )


    //this.stage.addEventListener( egret.FocusEvent.ACTIVATE, this.onActivate, this )
    // this.stage.addEventListener( egret.FocusEvent.DEACTIVATE, this.onDeactivate, this )


   this.stage.addEventListener( egret.TouchEvent.TOUCH_BEGIN, this.handleStageDown, this )
   this.stage.addEventListener( egret.TouchEvent.TOUCH_END, this.handleStageUp, this )
     this.stage.addEventListener( egret.TouchEvent.TOUCH_MOVE, this.handleStageMove, this )
     /*
     var shp:egret.Shape = new egret.Shape();
     shp.graphics.beginFill( 0xFF0000, 0.3 );
    shp.graphics.drawRect(0, 0, 200, 800);
    shp.graphics.endFill();
    this.addChild( shp );
    shp.touchEnabled = true;
    */
    
//shp.addEventListener( egret.TouchEvent.TOUCH_MOVE, this.handleStageMove, this )

    


   // Main.s_CurTown.SetCoins( 50000, true );

// poppin test
    CUIManager.s_uiCityHall.UnlockItems(1);


    this.addChild( Main.m_CloudManager.GetBubbleContainer() );

    // UI在一切之上，所以最后add进显示列表
    this.addChild( containerUI );
    this.addChild( CUIManager.s_containerUIs ); 


    this.addChild( CEffectManager.s_lstUiEffect );
    CEffectManager.Init();




      this.TestData();

    } // end CreateGameScene

    private onDeactivate( evt:egret.FocusEvent ):void
    {
       // console.log( "游戏失去焦点" );
         if ( Main.s_CurTown )
       {
           Main.s_CurTown.ProcessBankSaving_Begin();
       }

       
    }

    private onActivate( evt:egret.FocusEvent ):void
    {
       // console.log( "游戏获得焦点" );

       if ( Main.s_CurTown )
       {
           Main.s_CurTown.ProcessBankSaving_End();
       }

        
    }

    private handleTownDown( evt:egret.TouchEvent ):void
    { 

       // CSoundManager.PlayBMG();

    this.m_bDraggingStage = true;
      this.m_objectLastMousePosOnStage["x"] = evt.stageX;
      this.m_objectLastMousePosOnStage["y"] = evt.stageY;
      this.m_objectMouseDownPos["x"] = evt.stageX;
      this.m_objectMouseDownPos["y"] = evt.stageY;


    
    }

   private handleTownUp( evt:egret.TouchEvent ):void
    {
        if ( CUIManager.IsUiShowing() )
             {
                     return;
             }

     
         this.m_bDraggingStage = false;

         var huadongDeltaX = Math.abs (this.m_objectMouseDownPos["x"] -  evt.stageX);
         var huadongDeltaY = Math.abs (this.m_objectMouseDownPos["y"] -  evt.stageY);
        if ( ( huadongDeltaX >= 20 )||(  huadongDeltaY >= 20) )
        {
                console.log( "滑动了，不算点击：" );
                return;

        }   
        
     

    }

    private handleTownMove( evt:egret.TouchEvent ):void
    {
        return; // poppin test

        var deltaX:number = evt.stageX - this.m_objectLastMousePosOnStage["x"];
        var deltaY:number = evt.stageY - this.m_objectLastMousePosOnStage["y"];
        
      // console.log( "Delat: " + deltaX + "_" + deltaY );  
      if ( Main.s_CurTown != null )
      {
          Main.s_CurTown.DragScene( deltaX, deltaY );

          
      }

        this.m_objectLastMousePosOnStage["x"] = evt.stageX;
      this.m_objectLastMousePosOnStage["y"] = evt.stageY;
    }





    private m_bDraggingStage:boolean = false;
    private m_objectLastMousePosOnStage:Object = new Object();
    private m_objectMouseDownPos:Object = new Object();
    private handleStageDown( evt:egret.TouchEvent ):void
    {
         // CSoundManager.PlayBMG();


  if ( CUIManager.IsUiShowing() )
             {
                     return;
             }

       //  CTapModule.AddTapPoint( evt.touchPointID,  evt.stageX, evt.stageY );
      // this.m_timerDragScene.start();
      this.m_bDraggingStage = true;
      this.m_objectLastMousePosOnStage["x"] = evt.stageX;
      this.m_objectLastMousePosOnStage["y"] = evt.stageY;
      this.m_objectMouseDownPos["x"] = evt.stageX;
      this.m_objectMouseDownPos["y"] = evt.stageY;
      
    //  CUIManager.s_uiMainTitle.ShowDebugInfo( "Down: " + evt.touchPointID.toString() );
    }

    protected m_nEvtTouchPointId:number = 0;
    protected m_nEvtStageX:number = 0;
    protected m_nEvtStageY:number = 0;
    protected ReallyDo_HandleStageUp():void
    {
        if ( CUIManager.IsUiShowing() )
        {
            return;
        }

        CTapModule.RemovePoint( this.m_nEvtTouchPointId );
        //this.m_timerDragScene.stop();
         this.m_bDraggingStage = false;

         var huadongDeltaX = Math.abs (this.m_objectMouseDownPos["x"] -  this.m_nEvtStageX);
         var huadongDeltaY = Math.abs (this.m_objectMouseDownPos["y"] -  this.m_nEvtStageY);
        if ( ( huadongDeltaX >= 20 )||(  huadongDeltaY >= 20) )
        {
              //  console.log( "滑动了，不算点击：" );
                return;

        }   
        if ( Main.s_CurTown )
      {
          
          Main.s_CurTown.ProcessClick( this.m_nEvtStageX, this.m_nEvtStageY );
      }
    }

   

    private handleStageUp( evt:egret.TouchEvent ):void
    {
        this.m_nEvtTouchPointId = evt.touchPointID;
        this.m_nEvtStageX = evt.stageX;
        this.m_nEvtStageY = evt.stageY;
 
        this.m_timerPreHandleStageTap.start();
    }

    private handleStageMove( evt:egret.TouchEvent ):void
    {
          if ( CUIManager.IsUiShowing() )
             {
                     return;
             }

/*
              CTapModule.AddTapPoint( evt.touchPointID,  evt.stageX, evt.stageY );
   
     if ( CTapModule.TwoPointsTapToZoom() )
     {
         return;
     }
     
*/
         var deltaX:number = evt.stageX - this.m_objectLastMousePosOnStage["x"];
        var deltaY:number = evt.stageY - this.m_objectLastMousePosOnStage["y"];
        
      // console.log( "Delat: " + deltaX + "_" + deltaY );  
      if ( Main.s_CurTown != null )
      {
          Main.s_CurTown.DragScene( deltaX, deltaY );
      }

        this.m_objectLastMousePosOnStage["x"] = evt.stageX;
      this.m_objectLastMousePosOnStage["y"] = evt.stageY;


      console.log( "stage move.." );

    }
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    private createBitmapByName(name: string): egret.Bitmap {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }
    /**
     * 描述文件加载成功，开始播放动画
     * Description file loading is successful, start to play the animation
     */
    private startAnimation(result: Array<any>): void {
        let parser = new egret.HtmlTextParser();

        let textflowArr = result.map(text => parser.parse(text));
        let textfield = this.textfield;
        let count = -1;
        let change = () => {
            count++;
            if (count >= textflowArr.length) {
                count = 0;
            }
            let textFlow = textflowArr[count];

            // 切换描述内容
            // Switch to described content
            textfield.textFlow = textFlow;
            let tw = egret.Tween.get(textfield);
            tw.to({ "alpha": 1 }, 200);
            tw.wait(2000);
            tw.to({ "alpha": 0 }, 200);
            tw.call(change, this);
        };

        change();
    }

    /**
     * 点击按钮
     * Click the button
     */
    private onButtonClick(e: egret.TouchEvent) {
        let panel = new eui.Panel();
        panel.title = "Title";
        panel.horizontalCenter = 0;
        panel.verticalCenter = 0;
        this.addChild(panel);
    }
}
