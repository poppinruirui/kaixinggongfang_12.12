/*
    研发人员用的编辑器
*/
class CUIDeveloperEditor extends egret.DisplayObjectContainer {

    protected m_uiContainer:eui.Component = new eui.Component();

    protected m_listOperate:eui.List;
    protected m_listSelectTown:eui.List;
    protected m_btnZoomIn:eui.Button;
    protected m_btnZoomOut:eui.Button;
    protected m_btnSave:eui.Button;

     public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/CDeveloperEditor.exml";
        this.addChild( this.m_uiContainer );

        this.m_listOperate = this.m_uiContainer.getChildByName( "lstMainMenu" ) as eui.List;


        this.m_listSelectTown = this.m_uiContainer.getChildByName( "lstTownNo" ) as eui.List;
        this.m_listSelectTown.selectedIndex = 0;

        this.m_listSelectTown.dataProvider = new eui.ArrayCollection([
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
        
        ]);
this.m_listSelectTown.height  =1000;
     this.m_listSelectTown.addEventListener(eui.ItemTapEvent.ITEM_TAP,this.onListTownChange,this);


        this.m_listOperate.dataProvider = new eui.ArrayCollection([
        "do nothing",
    //    "打开车商城",
        "编辑建筑",
        "删除建筑",

        "增加钻石",
        "增加金币",

        "放置市政厅",
        "放置车站",
        "放置银行",
        "放置风车",
        "放置飞机场",
        "放置码头",

        "马路-左上右下",
        "马路-右上左下",
        "马路-转角-上",
        "马路-转角-下",
        "马路-转角-左",
        "马路-转角-右",
        "添加马路（新流程）",
        "添加测试车辆",
        "切换马路是否显示",
        "切换地块是否点选",
        "",

        "2*2型宗地",
        "4*4型宗地",

        "添加地表", 
        "删除地表", 

        "摆放树",

        "箭头左上",
        "箭头左下",
        "箭头右上",
        "箭头右下",

        ]);
   

   this.m_listOperate.addEventListener(eui.ItemTapEvent.ITEM_TAP,this.onListOperateChange,this);
   this.m_listOperate.height = 2000;

        this.m_listOperate.selectedIndex = 1;

         this.m_btnZoomIn = this.m_uiContainer.getChildByName( "btnZoomIn" ) as eui.Button;
        this.m_btnZoomIn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_ZoomIn, this);

        this.m_btnZoomOut = this.m_uiContainer.getChildByName( "btnZoomOut" ) as eui.Button;
        this.m_btnZoomOut.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_ZoomOut, this);

     // “保存”按钮
     this.m_btnSave = this.m_uiContainer.getChildByName( "btnSave" ) as eui.Button;
     this.m_btnSave.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Save, this);




     } // end constructor

      private onListOperateChange(e:eui.PropertyEvent):void{
        //获取点击消息
        
        if ( !CDef.EDITOR_MODE )
        {
            return;
        }

         Main.s_CurTown.SetOperate( this.m_listOperate.selectedIndex );
    }

    protected onListTownChange(e:eui.PropertyEvent):void{

        var nTownId:number = this.m_listSelectTown.selectedIndex + 1;
        Main.s_CurTown.ChangeTown( nTownId );

    }

public static s_fZoomAmount:number = 0.03;
protected onButtonClick_ZoomIn(e: egret.TouchEvent) {
    var fCurSize:number = Main.s_CurTown.scaleX;
    fCurSize += CUIDeveloperEditor.s_fZoomAmount;
    Main.s_CurTown.scaleX = fCurSize;
    Main.s_CurTown.scaleY = fCurSize;

    Main.m_CloudManager.Zoom( CUIDeveloperEditor.s_fZoomAmount, 1 );
}

protected onButtonClick_ZoomOut(e: egret.TouchEvent) {
    var fCurSize:number = Main.s_CurTown.scaleX;
    fCurSize -= CUIDeveloperEditor.s_fZoomAmount;
    Main.s_CurTown.scaleX = fCurSize;
    Main.s_CurTown.scaleY = fCurSize;

    Main.m_CloudManager.Zoom( CUIDeveloperEditor.s_fZoomAmount, -1 );
}

protected  onButtonClick_Save(e: egret.TouchEvent) {
   var szData:string = Main.s_CurTown.GenerateData();
   egret.localStorage.setItem("Town" + Main.s_CurTown.GetId(), szData );       
}

} // end class