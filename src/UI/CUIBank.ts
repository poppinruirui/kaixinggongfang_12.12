class CUIBank extends egret.DisplayObjectContainer {

    protected m_uiContainer:eui.Component = new eui.Component();
    protected m_btnClose:CCosmosImage;

    protected m_txtMainValue:eui.Label;
    protected m_txtBranchProfit:eui.Label;

    protected m_btnUpgrade:CCosmosImage;

    public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/CBank.exml";
        this.addChild( this.m_uiContainer );

        this.m_txtMainValue = this.m_uiContainer.getChildByName( "txt3" ) as eui.Label;
        this.m_txtBranchProfit = this.m_uiContainer.getChildByName( "txt5" ) as eui.Label;
        
        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img1" ) as eui.Image;
        CColorFucker.SetImageColor( imgTemp, 193, 255, 193 );

        imgTemp = this.m_uiContainer.getChildByName( "img4" ) as eui.Image;
        CColorFucker.SetImageColor( imgTemp, 190, 190, 190 );

        imgTemp = this.m_uiContainer.getChildByName( "img3" ) as eui.Image;
        CColorFucker.SetImageColor( imgTemp, 211, 211, 211 );

        imgTemp = this.m_uiContainer.getChildByName( "img6" ) as eui.Image;
        this.m_btnUpgrade = new CCosmosImage();
        this.m_uiContainer.addChild( this.m_btnUpgrade );
        this.m_btnUpgrade.SetExml( "resource/assets/MyExml/CCosmosButton6.exml" );
        this.m_btnUpgrade.x = imgTemp.x;
        this.m_btnUpgrade.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnUpgrade.SetImageColor_New( 2, 255, 127, 36 );
        this.m_btnUpgrade.SetImageColor_New( 3, 255, 215, 0 );
        this.m_btnUpgrade.SetImageColor_New( 1, 211, 211, 211 );
        this.m_btnUpgrade.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_UpGrade, this);

        imgTemp = this.m_uiContainer.getChildByName( "img5" ) as eui.Image;
        this.m_btnClose = new CCosmosImage();
        this.m_btnClose.SetExml( "resource/assets/MyExml/CCosmosButton1.exml" );
        this.m_uiContainer.addChild( this.m_btnClose );
        this.m_btnClose.x = imgTemp.x;
        this.m_btnClose.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnClose.UseColorSolution(0);
        this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);



    } // constructor

        protected onButtonClick_Close( evt:egret.TouchEvent ):void
       {
         CUIManager.SetUiVisible( Global.eUiId.bank, false );
       }


       public UpdateStatus():void
       {
            var data:CBankData = Main.s_CurTown.GetBankData();

            this.m_txtMainValue.text = "离开时自动产出，" + 
                CConfigManager.GenShowStyle( Global.eValueShowStyle.percent, data.nCurProfitRate ) +
                " 持续 " +
                data.nCurAvailableTime + "分钟";
            ;

            var nCurCost:number = CConfigManager.GetBankCostByLevel( data.nCurLevel );
            nCurCost = Math.floor( nCurCost );
            this.m_btnUpgrade.SetText( 1, nCurCost.toString());

            if ( CPlayer.GetMoney( Global.eMoneyType.coin ) < nCurCost )
            {
                this.m_btnUpgrade.SetEnabled( false );
            } 
            else
            {
                this.m_btnUpgrade.SetEnabled( true );
            }
   
       }

       protected onButtonClick_UpGrade( evt:egret.TouchEvent ):void
       {
           var data:CBankData = Main.s_CurTown.GetBankData();
           var nCurCost:number = CConfigManager.GetBankCostByLevel( data.nCurLevel );
            if ( CPlayer.GetMoney( Global.eMoneyType.coin ) < nCurCost )
            {
                console.log( "有bug，金币不够时按钮应该灰掉才对，" );
                return;
            }
            
            // 消费金币
            CPlayer.SetMoney( Global.eMoneyType.coin, CPlayer.GetMoney( Global.eMoneyType.coin) -  nCurCost );

            data.SetCurLevel( data.GetCurLevel() + 1 );

            // 刷新界面状态
            this.UpdateStatus();
       }


} // end class