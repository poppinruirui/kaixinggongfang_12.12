/*
“车库”界面中，列表中的一条记录
*/
class CUIGarageItem extends egret.DisplayObjectContainer {

    protected m_bmpBg:eui.Image;
    protected m_bmpGrayMask:egret.Bitmap;
    protected m_bmpCarAvatar:egret.Bitmap;
    protected m_txtCarName:eui.Label;
    protected m_txtCarDesc:eui.Label;
    protected m_txtPrice:eui.Label;

    protected m_txtBuyTitle:eui.Label;

    protected m_btnUnlock:eui.Image;

    protected m_nIndex:number = 0;

    protected m_eStatus:Global.eGarageItemStatus = Global.eGarageItemStatus.locked_and_can_unlock;

    protected m_uiContainer:eui.Component = new eui.Component();

    public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/GarargeItem.exml";
        this.addChild( this.m_uiContainer );

        this.m_txtCarName = this.m_uiContainer.getChildByName( "txtName" ) as eui.Label;  
        this.m_txtCarDesc = this.m_uiContainer.getChildByName( "txtDesc" ) as eui.Label;  
        this.m_bmpCarAvatar = this.m_uiContainer.getChildByName( "imgAvatar" ) as eui.Image;  
        this.m_bmpGrayMask = this.m_uiContainer.getChildByName( "imgMask" ) as eui.Image;  
        this.m_bmpGrayMask.visible = true;
        this.m_btnUnlock = this.m_uiContainer.getChildByName( "btnBuy" ) as eui.Image;  
        this.m_btnUnlock.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_UnlockThisCar, this);
      //  this.SetUnlockButtonEnabled( false );

        this.m_txtPrice = this.m_uiContainer.getChildByName( "txtPrice" ) as eui.Label;  
        this.m_txtBuyTitle = this.m_uiContainer.getChildByName( "txtBuyTitle" ) as eui.Label;  
         
        
/*
        this.m_bmpBg = new eui.Image( RES.getRes( "1X1_png" ) );
        this.m_bmpBg.touchEnabled = true;
        this.addChild( this.m_bmpBg );
        this.m_bmpBg.width = CDef.s_fGarageItemWitdh;
        this.m_bmpBg.height = CDef.s_fGarageItemHeight;
        this.m_bmpBg.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 0.262, 0.262, 0.262,1 )];

  



        this.m_bmpCarAvatar = new eui.Image();
        this.m_bmpCarAvatar.x = 5;
        this.m_bmpCarAvatar.y = 10;
        this.addChild( this.m_bmpCarAvatar );

        this.m_txtCarName = new eui.Label();
        this.m_txtCarName.x = 75;
        this.m_txtCarName.y = 15;
        this.m_txtCarName.size = 24;
        this.m_txtCarName.bold = true;
        this.addChild( this.m_txtCarName );

        this.m_txtCarDesc = new eui.Label();
        this.m_txtCarDesc.x = 80;
        this.m_txtCarDesc.y = 50;
        this.m_txtCarDesc.size = 20;
        this.m_txtCarDesc.textColor = 0xCBCBCB;
        this.addChild( this.m_txtCarDesc );

        this.m_btnUnlock = new CUIBaseCommonButton2();
        this.addChild( this.m_btnUnlock );
        this.m_btnUnlock.x = 385;
         this.m_btnUnlock.y = 10;
       


        this.m_bmpGrayMask = new eui.Image( RES.getRes( "1X1_png" ) );
        this.addChild( this.m_bmpGrayMask );
        this.m_bmpGrayMask.width = CDef.s_fGarageItemWitdh;
        this.m_bmpGrayMask.height = CDef.s_fGarageItemHeight;
        this.m_bmpGrayMask.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 0.412, 0.412, 0.412, 0.7 )];
        this.m_bmpGrayMask.alpha = 0.7;
*/

    } // end constructor

    public SetIndex( nIndex:number ):void
    {   
        this.m_nIndex = nIndex;

        var szResUrl:string  = CAutomobile.GetCarResUrlByIdAndDir( nIndex, 1 );
         RES.getResByUrl( szResUrl, this.onLoadCarResComplete, this, RES.ResourceItem.TYPE_IMAGE);

/*
        this.m_bmpCarAvatar.texture = RES.getRes( "car_" + nIndex  + "_0_png" );
        this.m_bmpCarAvatar.scaleX = 0.32;
        this.m_bmpCarAvatar.scaleY = 0.32;
*/
        var car_config =CConfigManager.GetCarConfig(nIndex);
        if ( car_config !=  null )
        {
            this.m_txtCarName.text = car_config.szName;
           this.m_txtCarDesc.text = car_config.szDesc;
        }

      
    }

     protected onLoadCarResComplete(event:any):void {
      
        var tex: egret.Texture = <egret.Texture>event;
       this.m_bmpCarAvatar.texture = tex;

          this.anchorOffsetX = this.m_bmpCarAvatar.width / 2;
                this.anchorOffsetY = this.m_bmpCarAvatar.height / 2;
     }

    public GetIndex():number
    {
        return this.m_nIndex;
    }

    public SetLock( bLock:boolean ):void
    {
        this.m_btnUnlock.touchEnabled = !bLock;
    }

    public onButtonClick_UnlockThisCar( evt:egret.TouchEvent ):void
    {
        Main.s_CurTown.AddOneCar( this.GetIndex() );
        this.m_btnUnlock.visible = false;
        Main.s_CurTown.SetGarageItemStatus( this.GetIndex(), Global.eGarageItemStatus.unlocked );
        Main.s_CurTown.CostCoinDueToUnlockCar( this.GetIndex() );

        

        Main.s_CurTown.UpdateGarageStatus();

        CUIManager.s_uiGarage.SetMaskVisible( this.GetIndex() + 1, false );
    }

    public SetMaskVisible( bVisible:boolean ):void
    {
        this.m_bmpGrayMask.visible = bVisible;
    }

    public SetUnlockButtonEnabled( bEnabled:boolean ):void
    {
       
        if (  bEnabled)
        {
            this.m_btnUnlock.filters = null;
           
            if (!this.m_btnUnlock.hasEventListener( egret.TouchEvent.TOUCH_TAP ))
            {
                this.m_btnUnlock.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_UnlockThisCar, this);
            }
        }
        else
        {
            if (this.m_btnUnlock.hasEventListener( egret.TouchEvent.TOUCH_TAP ))
            {
              this.m_btnUnlock.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_UnlockThisCar, this);
            }

            this.m_btnUnlock.filters = [ CColorFucker.GetDisabledGray() ];
        }

    }

    public SetStatus( eStatus:Global.eGarageItemStatus ):void
    {
                this.m_eStatus = eStatus;
        switch( this.m_eStatus )
        {
            case Global.eGarageItemStatus.can_not_unlock:
            {
                this.m_bmpGrayMask.visible = true;
                this.SetUnlockButtonEnabled( false );
                this.m_btnUnlock.visible = false;

                this.m_txtPrice.visible = false;
                this.m_txtBuyTitle.visible =false;
                
                this.m_bmpGrayMask.visible = true;
            }
            break;
            case Global.eGarageItemStatus.locked_and_can_unlock:
            {
                this.m_bmpGrayMask.visible = false;
               
                this.m_btnUnlock.visible = true;
               this.SetUnlockButtonEnabled( true );

                this.m_txtPrice.visible = true;
                this.m_txtBuyTitle.visible =true;


            }
            break;
            case Global.eGarageItemStatus.unlocked:
            {
                 this.m_bmpGrayMask.visible = false;
                this.m_btnUnlock.visible = false;

                 this.m_txtPrice.visible = false;
                this.m_txtBuyTitle.visible =false;
            }
            break;
        }
        // end switch

        this.m_txtPrice.text =  CConfigManager.GetCoinForUnlockCar( this.GetIndex() ).toString();

    }




} // end class