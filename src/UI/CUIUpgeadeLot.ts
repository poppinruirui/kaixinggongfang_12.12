class CUIUpgeadeLot extends egret.DisplayObjectContainer {

     protected m_uiContainer:eui.Component = new eui.Component();
     protected m_btnClose:CCosmosImage;

     protected m_labelBuildingName:eui.Label;
     protected m_labelCurLotLevel:eui.Label;
     protected m_labelCurLotCPS:eui.Label;

     protected m_btnRevert:CCosmosImage;
     protected m_btnInstantUpgrade:CCosmosImage;
     protected m_btnMannual:CCosmosImage;

     protected m_imgBuildingAvatar:eui.Image;

     protected m_cbHistorical:eui.CheckBox;

     public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/CUpgradePanel.exml";
        this.addChild( this.m_uiContainer );

        var imgBtnRevert:eui.Image = this.m_uiContainer.getChildByName( "btnRevert" ) as eui.Image;
        imgBtnRevert.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Revert, this);

        var imgBtnClose:eui.Image  = this.m_uiContainer.getChildByName( "btnClose" ) as eui.Image;
        imgBtnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);

        var imgBtnUseGreenCashUpgrade:eui.Image = this.m_uiContainer.getChildByName( "BtnUseGreenCashUpgrade" ) as eui.Image;
        imgBtnUseGreenCashUpgrade.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_InstantUpgrade, this);

        this.m_imgBuildingAvatar = this.m_uiContainer.getChildByName( "imgAvatar" ) as eui.Image;
        this.m_labelBuildingName = this.m_uiContainer.getChildByName( "labelBuildingName" ) as eui.Label;
        this.m_labelCurLotLevel = this.m_uiContainer.getChildByName( "labelBuildingLevel" ) as eui.Label;
        this.m_labelCurLotCPS = this.m_uiContainer.getChildByName( "labelBuildingCPS" ) as eui.Label;
/*
        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "btnClose" ) as eui.Image;
        this.m_btnClose = new CCosmosImage();
        this.m_btnClose.SetExml( "resource/assets/MyExml/CCosmosButton1.exml" );
        this.m_uiContainer.addChild( this.m_btnClose );
        this.m_btnClose.x = imgTemp.x;
        this.m_btnClose.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnClose.UseColorSolution(0);
        this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);

        this.m_labelCurLotLevel = this.m_uiContainer.getChildByName( "labelBuildingLevel" ) as eui.Label;
        this.m_labelBuildingName = this.m_uiContainer.getChildByName( "labelBuildingName" ) as eui.Label;
        this.m_labelCurLotCPS = this.m_uiContainer.getChildByName( "labelBuildingCPS" ) as eui.Label;

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "btnRevert" ) as eui.Image;
        this.m_btnRevert = new CCosmosImage();
        this.m_uiContainer.addChild(this.m_btnRevert);
        this.m_btnRevert.SetExml( "resource/assets/MyExml/CCosmosButton2.exml" );
        this.m_btnRevert.x = imgTemp.x;
        this.m_btnRevert.y = imgTemp.y;
        this.m_btnRevert.scaleX = imgTemp.scaleX;
        this.m_btnRevert.scaleY = imgTemp.scaleY;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnRevert.SetImageColor_255( null, 1, 179, 58, 43, 255 )
        this.m_btnRevert.SetImageColor_255( null, 2, 235, 77, 58, 255 )
        var tex:egret.Texture = RES.getRes( "bulldoze_png" );
        this.m_btnRevert.SetImageColor_255( tex, 4, 255, 255, 255, 255, false );
        this.m_btnRevert.SetLabelContent( 0, "" );
        this.m_btnRevert.SetLabelContent( 1, "" );
        this.m_btnRevert.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Revert, this);

        imgTemp = this.m_uiContainer.getChildByName( "btnInstantUpgrade" ) as eui.Image;
        this.m_btnInstantUpgrade = new CCosmosImage();
        this.m_uiContainer.addChild(this.m_btnInstantUpgrade);
        this.m_btnInstantUpgrade.SetExml( "resource/assets/MyExml/CCosmosButton2.exml" );
        this.m_btnInstantUpgrade.x = imgTemp.x;
        this.m_btnInstantUpgrade.y = imgTemp.y;
        this.m_btnInstantUpgrade.scaleX = imgTemp.scaleX;
        this.m_btnInstantUpgrade.scaleY = imgTemp.scaleY;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnInstantUpgrade.SetImageColor_255( null, 1, 242, 166, 58, 255 )
        this.m_btnInstantUpgrade.SetImageColor_255( null, 2, 247, 206, 70, 255 )
        var tex:egret.Texture = RES.getRes( "hammer_png" );
        this.m_btnInstantUpgrade.SetImageColor_255( tex, 4, 255, 255, 255, 255, false );
        this.m_btnInstantUpgrade.SetLabelContent( 0, "" );
        this.m_btnInstantUpgrade.SetLabelContent( 1, "" );
        this.m_btnInstantUpgrade.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_InstantUpgrade, this);

        //// “手动摆放”按钮
        imgTemp = this.m_uiContainer.getChildByName( "btnMannual" ) as eui.Image;
        this.m_btnMannual = new CCosmosImage();
      //  this.m_uiContainer.addChild(this.m_btnMannual);
        this.m_btnMannual.SetExml( "resource/assets/MyExml/CCosmosButton2.exml" );
        this.m_btnMannual.x = imgTemp.x;
        this.m_btnMannual.y = imgTemp.y;
        this.m_btnMannual.scaleX = imgTemp.scaleX;
        this.m_btnMannual.scaleY = imgTemp.scaleY;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnMannual.SetImageColor_255( null, 1, 242, 166, 58, 255 )
        this.m_btnMannual.SetImageColor_255( null, 2, 247, 206, 70, 255 )
        var tex:egret.Texture = RES.getRes( "hammer_png" );
        this.m_btnMannual.SetImageColor_255( tex, 4, 255, 255, 255, 255, false );
        this.m_btnMannual.SetLabelContent( 0, "" );
        this.m_btnMannual.SetLabelContent( 1, "" );
        this.m_btnMannual.SetLabelContent( 2, "" );
        this.m_btnMannual.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_BuildingsPanel, this);

        
        //// end “手动摆放”按钮




        imgTemp = this.m_uiContainer.getChildByName( "imgBgMask" ) as eui.Image;
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(188, 176, 56)];

      imgTemp = this.m_uiContainer.getChildByName( "img2" ) as eui.Image;
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(221, 221, 221)];

        this.m_imgBuildingAvatar = this.m_uiContainer.getChildByName( "imgAvatar" ) as eui.Image;

        this.m_btnRevert.SetText( 2, "推平成空地" );
     

         this.m_btnInstantUpgrade.SetText( 3, "1" );
         this.m_btnInstantUpgrade.SetText( 2, "" );
         this.m_btnInstantUpgrade.SetImageTexture( 5, RES.getRes( "zuanshi_png" ) );

         this.m_cbHistorical = this.m_uiContainer.getChildByName( "cbHistorical" ) as eui.CheckBox;
         this.m_cbHistorical.addEventListener( eui.UIEvent.CHANGE, this.onCheckBoxValueChange, this );
         */

        var img0:eui.Image = this.m_uiContainer.getChildByName( "img0" ) as eui.Image;
        img0.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 0, 0, 0 ) ];
        img0.alpha = 0.6;

     } // end constructor

     protected onCheckBoxValueChange( evt:eui.UIEvent ):void
     {
         var obj:CObj = Main.s_CurTown.GetCurEditProcessingLot();
         obj.SetHistorical( this.IsHistorical() );
     }

    private onButtonClick_Close( evt:egret.TouchEvent ):void
    {
          CUIManager.SetUiVisible( Global.eUiId.lot_upgrade, false );

          Main.s_CurTown.SetCurEditProcessingLot( null );
    }

    public SetCurLotLevel( nCurLotLevel:number ):void
    {
        this.m_labelCurLotLevel.text = nCurLotLevel + "级";
    }

    public SetCurLotCPS( nCurLotCPS:number ):void
    {
          this.m_labelCurLotCPS.text = nCurLotCPS.toString();
        
    }

    public SetBuildingAvatar( tex:egret.Texture ):void
    {
        this.m_imgBuildingAvatar.texture = tex;
    }

    public SetBuildingName( szBuildingName:string ):void
    {
        this.m_labelBuildingName.text = szBuildingName;
    }

    protected onButtonClick_BuildingsPanel( evt:egret.TouchEvent ):void
    {
         CUIManager.SetUiVisible( Global.eUiId.lot_upgrade, false );
         CUIManager.SetUiVisible( Global.eUiId.special_panel, true );
         CUIManager.s_uiSpecialPanel.UpdateInfo();
    }

    protected onButtonClick_Revert( evt:egret.TouchEvent ):void
    {
        CUIManager.SetUiVisible( Global.eUiId.revert_lot_confirm, true );
    }

    protected onButtonClick_InstantUpgrade( evt:egret.TouchEvent ):void
    {
        var lotEditing:CObj = Main.s_CurTown.GetCurEditProcessingLot();  
 CSoundManager.PlaySE( Global.eSE.upgrad_complete );



        lotEditing.SetLotLevel( lotEditing.GetLotLevel() + 1 );

        this.SetCurLotLevel( lotEditing.GetLotLevel() );
 
       

        var nNeedDiamond:number = CConfigManager.GetNeedDiamondBySizeAndLevel( lotEditing.GetSizeType(), lotEditing.GetLotLevel() );
        nNeedDiamond = Math.floor( nNeedDiamond );
        if ( nNeedDiamond > CPlayer.GetDiamond() )
        {
            console.log( "有Bug" );
            return;
        }
        CPlayer.SetDiamond( CPlayer.GetDiamond() - nNeedDiamond );

        this.UpdateInstanceUpgradeButtonStatus();

         Main.s_CurTown.UpdateCurLotStatus();
        Main.s_CurTown.OnBuildingCPSChanged();
    }

    public UpdateInstanceUpgradeButtonStatus():void
    {
        var lotEditing:CObj = Main.s_CurTown.GetupgradinLot();  
        var lotUpgrading:CObj = Main.s_CurTown.GetCurEditProcessingLot();
        if ( lotEditing == null )
        {
            return;
        }
        
        var nNeedDiamond:number = CConfigManager.GetNeedDiamondBySizeAndLevel( lotEditing.GetSizeType(), lotEditing.GetLotLevel() );
        nNeedDiamond = Math.floor( nNeedDiamond );
        if ( nNeedDiamond > CPlayer.GetDiamond() )
        {
            this.m_btnInstantUpgrade.SetEnabled( false );
        }
        else
        {
            this.m_btnInstantUpgrade.SetEnabled( true );
        }
        this.m_btnInstantUpgrade.SetText( 3, nNeedDiamond.toString() );
    }

    public IsHistorical():boolean
    {
        return this.m_cbHistorical.selected;
    }

    public UpdatePanelStatusWhenOpen():void
    {
        var obj:CObj = Main.s_CurTown.GetCurEditProcessingLot();
     //   this.m_cbHistorical.selected = obj.GetHistorical();
    }



} // end class