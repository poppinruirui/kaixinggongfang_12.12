class CUISpecailPanel extends egret.DisplayObjectContainer {



     protected m_uiContainer:eui.Component = new eui.Component();
     protected m_btnClose:CCosmosImage;

     protected m_nCurPageIndex:number = 0;

     protected m_dicBuildingData:Object = new Object();
     protected m_dicBuildingIndex:Object = new Object();
     protected m_CurBuildingData:CConfigBuilding = null;
     protected m_dicPageContainer:Object = new Object();
     protected m_btnSwitchPage0:CCosmosImage;
     protected m_btnSwitchPage1:CCosmosImage;
     protected m_btnSwitchPage2:CCosmosImage;

     protected m_imgAvatar:eui.Image;

     protected m_txtBuildingName:eui.Label;
     protected m_txtBuildingNumber:eui.Label;
     protected m_txtBuildingPropertyType:eui.Label;
     protected m_txtBuildingBoostRate:eui.Label;

     protected m_btnPrev:CCosmosImage;
     protected m_btnNext:CCosmosImage;

     protected m_btnUnlock:CCosmosImage;

     protected m_txtCurProcessingLotType:eui.Label;

      public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/CSpecailPanel.exml";
        this.addChild( this.m_uiContainer );

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img2" ) as eui.Image;
        this.m_btnSwitchPage0 = new CCosmosImage();
        this.m_btnSwitchPage0.SetExml( "resource/assets/MyExml/CCosmosSimpleButton.exml" );
        this.m_uiContainer.addChild( this.m_btnSwitchPage0 );
        this.m_btnSwitchPage0.x = imgTemp.x;
        this.m_btnSwitchPage0.y = imgTemp.y;
        this.m_btnSwitchPage0.scaleX = imgTemp.scaleX;
        this.m_btnSwitchPage0.scaleY = imgTemp.scaleY;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnSwitchPage0.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Page0, this);
        this.m_btnSwitchPage0.SetTextColor( 0, 0xFFFFFF );
        this.m_dicPageContainer[0] = this.m_btnSwitchPage0;

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img3" ) as eui.Image;
        this.m_btnSwitchPage1 = new CCosmosImage();
        this.m_btnSwitchPage1.SetExml( "resource/assets/MyExml/CCosmosSimpleButton.exml" );
        this.m_uiContainer.addChild( this.m_btnSwitchPage1 );
        this.m_btnSwitchPage1.x = imgTemp.x;
        this.m_btnSwitchPage1.y = imgTemp.y;
        this.m_btnSwitchPage1.scaleX = imgTemp.scaleX;
        this.m_btnSwitchPage1.scaleY = imgTemp.scaleY;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnSwitchPage1.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Page1, this);
        this.m_btnSwitchPage1.SetTextColor( 0, 0xFFFFFF );
        this.m_dicPageContainer[1] = this.m_btnSwitchPage1;

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img4" ) as eui.Image;
        this.m_btnSwitchPage2 = new CCosmosImage();
        this.m_btnSwitchPage2.SetExml( "resource/assets/MyExml/CCosmosSimpleButton.exml" );
        this.m_uiContainer.addChild( this.m_btnSwitchPage2 );
        this.m_btnSwitchPage2.x = imgTemp.x;
        this.m_btnSwitchPage2.y = imgTemp.y;
        this.m_btnSwitchPage2.scaleX = imgTemp.scaleX;
        this.m_btnSwitchPage2.scaleY = imgTemp.scaleY;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnSwitchPage2.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Page2, this);
        this.m_btnSwitchPage2.SetTextColor( 0, 0xFFFFFF );
        this.m_dicPageContainer[2] = this.m_btnSwitchPage2;

        imgTemp = this.m_uiContainer.getChildByName( "btnClose" ) as eui.Image;
        this.m_btnClose = new CCosmosImage();
        this.m_btnClose.SetExml( "resource/assets/MyExml/CCosmosButton1.exml" );
        this.m_uiContainer.addChild( this.m_btnClose );
        this.m_btnClose.x = imgTemp.x;
        this.m_btnClose.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnClose.UseColorSolution(0);
        this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);

        imgTemp = this.m_uiContainer.getChildByName( "img5" ) as eui.Image;
        imgTemp.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255(118, 238, 0) ];


        imgTemp = this.m_uiContainer.getChildByName( "img6" ) as eui.Image;
        this.m_btnPrev = new CCosmosImage();
        this.m_btnPrev.SetExml( "resource/assets/MyExml/CCosmosSimpleButton.exml" );
        this.m_btnPrev.x = imgTemp.x;
        this.m_btnPrev.y = imgTemp.y;
        this.m_uiContainer.addChild( this.m_btnPrev );
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnPrev.SetLabelContent( 0, "" );
        this.m_btnPrev.SetImageTexture( 0, RES.getRes( "arrow1_png" ) );
        this.m_btnPrev.width = 100;
        this.m_btnPrev.height = 100;
        this.m_btnPrev.SetImageSize( 0, 100, 100 );
         this.m_btnPrev.SetLabelVisible(0, false );
        this.m_btnPrev.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Prev, this);

        imgTemp = this.m_uiContainer.getChildByName( "img8" ) as eui.Image;
        this.m_btnNext = new CCosmosImage();
        this.m_btnNext.SetExml( "resource/assets/MyExml/CCosmosSimpleButton.exml" );
        this.m_btnNext.x = imgTemp.x;
        this.m_btnNext.y = imgTemp.y;
        this.m_uiContainer.addChild( this.m_btnNext );
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnNext.SetLabelContent( 0, "" );
        this.m_btnNext.SetLabelVisible(0, false );
        this.m_btnNext.SetImageTexture( 0, RES.getRes( "arrow1_png" ) );
        this.m_btnNext.width = 100;
        this.m_btnNext.height = 100;
        this.m_btnNext.SetImageSize( 0, 100, 100 );
        this.m_btnNext.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Next, this);

        this.m_txtBuildingName = this.m_uiContainer.getChildByName( "txt0" ) as eui.Label;
        this.m_txtBuildingNumber = this.m_uiContainer.getChildByName( "txt1" ) as eui.Label;
        this.m_txtBuildingPropertyType = this.m_uiContainer.getChildByName( "txt2" ) as eui.Label;
        this.m_txtBuildingBoostRate = this.m_uiContainer.getChildByName( "txt3" ) as eui.Label;
        this.m_txtCurProcessingLotType = this.m_uiContainer.getChildByName( "txt4" ) as eui.Label;

        this.m_imgAvatar = this.m_uiContainer.getChildByName( "img1" ) as eui.Image;
        
        imgTemp = this.m_uiContainer.getChildByName( "img9" ) as eui.Image;
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255( 238, 233, 233 )];

        imgTemp = this.m_uiContainer.getChildByName( "img7" ) as eui.Image;
        this.m_btnUnlock = new CCosmosImage();
        this.m_btnUnlock.SetExml( "resource/assets/MyExml/CCosmosButton6.exml" );
        this.m_btnUnlock.x = imgTemp.x;
        this.m_btnUnlock.y = imgTemp.y;
        this.m_uiContainer.addChild( this.m_btnUnlock );
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnUnlock.SetLabelContent( 0, "aaaaaa" );
        this.m_btnUnlock.scaleX  =1.5;
        this.m_btnUnlock.scaleY  =1.5;
        this.m_btnUnlock.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_UnlockOrUse, this);
        this.m_btnUnlock.SetTextColor( 0, 0xFFFFFF );
        this.m_btnUnlock.SetTextColor( 1, 0x000000 );
        this.m_btnUnlock.SetImageColor_New( 1, 190, 190, 190 );
        this.m_btnUnlock.SetImageColor_New( 2, 148, 0, 211 );
        this.m_btnUnlock.SetImageColor_New( 3, 186, 85, 211 );
        this.m_btnUnlock.SetImageTexture( 4, RES.getRes( "zuanshi_png" ) );

        this.m_dicBuildingIndex[0] = 0;
        this.m_dicBuildingIndex[1] = 0;
        this.m_dicBuildingIndex[2] = 0;


      } // end constructor

      private onButtonClick_Close( evt:egret.TouchEvent ):void
      {
          CUIManager.SetUiVisible( Global.eUiId.special_panel, false );
      }

      protected onButtonClick_Next( evt:egret.TouchEvent ):void
      {
          var nCurBuildingIndex:number = this.GetCurBuildingIndex();
          nCurBuildingIndex++;
          if ( nCurBuildingIndex > this.GetMaxBuildingIndex() )
          {
              nCurBuildingIndex = 0;
          }
          this.SetCurBuildingIndex( nCurBuildingIndex );
          this.SelectBuilding();
      }

      protected onButtonClick_Prev( evt:egret.TouchEvent ):void
      {
          var nCurBuildingIndex:number = this.GetCurBuildingIndex();
          nCurBuildingIndex--;
          if ( nCurBuildingIndex < 0 )
          {
              nCurBuildingIndex = this.GetMaxBuildingIndex();
          }
          this.SetCurBuildingIndex( nCurBuildingIndex );
          this.SelectBuilding();
      }

      public SwitchPage( nPageIndex:number ):void
      {
          this.m_nCurPageIndex = nPageIndex;

          for ( var i:number = 0; i < 3; i++ )
          {
              var btnPage:CCosmosImage = this.m_dicPageContainer[i] as CCosmosImage;

              if ( i ==  nPageIndex )
              {
                  btnPage.SetImageColor_New( 0, 25, 25, 25 );
              }
              else
              {
                  btnPage.SetImageColor_New( 0, 190, 190, 190 );
              }

          }

          if ( this.m_nCurPageIndex == 0 || this.m_nCurPageIndex == 1 )
          {
              this.m_btnUnlock.SetText( 0, "使用" );
              this.m_btnUnlock.SetImageVisible( 4, false );
              this.m_btnUnlock.SetText( 1, "" );
          }
          else
          {
              this.m_btnUnlock.SetText( 0, "解锁" );
              this.m_btnUnlock.SetImageVisible( 4, true );
              this.m_btnUnlock.SetText( 1, "" );
          }





          this.SelectBuilding();
      }

      public SelectBuilding():void
      {
          // 先把上一轮显示结果清空
          this.m_txtBuildingName.text = "";
          this.m_txtBuildingNumber.text = "";
          this.m_txtBuildingPropertyType.text = "";
          this.m_txtBuildingBoostRate.text = "";
          this.m_imgAvatar.texture = null;
     
          this.m_CurBuildingData = null;

          var aryBuildingsData:Array<CConfigBuilding> = this.GetBuildingsDataByPageIndex( this.m_nCurPageIndex );
          if ( aryBuildingsData.length == 0 )
          {
              this.m_btnUnlock.visible = false;
              return;
          }
          this.m_btnUnlock.visible = true;

          this.m_CurBuildingData = aryBuildingsData[this.GetCurBuildingIndex()];
          this.m_txtBuildingName.text = this.m_CurBuildingData.szName;
          this.m_imgAvatar.texture = RES.getRes( this.m_CurBuildingData.szResName );
          this.m_txtBuildingNumber.text = ( this.GetCurBuildingIndex() + 1 ) + " / " + ( this.GetMaxBuildingIndex() + 1);
          this.m_txtBuildingPropertyType.text = CBuildingManager.PropertyType2String( this.m_CurBuildingData.ePropertyType );
          this.m_txtBuildingBoostRate.text = ( this.m_CurBuildingData.fGainRate * 100 ) + "%";

          if ( this.m_nCurPageIndex == 0 || this.m_nCurPageIndex == 1 ) // 使用
          {
             var bCanUse:boolean = true;

             if (  Main.s_CurTown.GetCurEditProcessingLot().GetLotProperty() !=  this.m_CurBuildingData.ePropertyType )
             {
                  this.m_btnUnlock.SetText( 1, "属性不符" );
                  bCanUse = false;

             }

             if ( this.m_CurBuildingData.bSpecial )
             {
                 if ( this.m_CurBuildingData.bUsed )
                 {
                    this.m_btnUnlock.SetText( 1, "已使用" );
                    bCanUse = false;
 
                 }
             }
             
             if ( bCanUse )
             {
                 this.m_btnUnlock.SetText(1, "");
             }
             this.m_btnUnlock.SetEnabled( bCanUse );
             
          }
          else // 解锁
          {
            this.m_btnUnlock.SetText( 1, this.m_CurBuildingData.nPrice.toString() );
            if ( CPlayer.GetDiamond() >= this.m_CurBuildingData.nPrice )
            {
                this.m_btnUnlock.SetEnabled( true );
            }
            else
            {
                this.m_btnUnlock.SetEnabled( false );
            }
          }



      }

      public GetMaxBuildingIndex():number
      {
          return (this.m_dicBuildingData[this.m_nCurPageIndex] as Array<CConfigBuilding> ).length - 1;
      }

      public GetCurBuildingIndex():number
      {
            return this.m_dicBuildingIndex[this.m_nCurPageIndex];
      }

      public SetCurBuildingIndex( nCurBuildingIndex:number ):void
      {
            this.m_dicBuildingIndex[this.m_nCurPageIndex] = nCurBuildingIndex;
      }

      protected onButtonClick_Page0( evt:egret.TouchEvent ):void
      {
          this.SwitchPage(0);
      }

      protected onButtonClick_Page1( evt:egret.TouchEvent ):void
      {
          this.SwitchPage(1);
      }

      protected onButtonClick_Page2( evt:egret.TouchEvent ):void
      {
            this.SwitchPage(2);
      }

      public GetBuildingsDataByPageIndex( nPageIndex:number ):Array<CConfigBuilding>
      {
          return this.m_dicBuildingData[nPageIndex];
      }

      public UpdateInfo():void
      {
          CBuildingManager.LoadConfig();
          

          this.m_dicBuildingData[0] = CBuildingManager.GetCommonBuildings();
          this.m_dicBuildingData[1] = CBuildingManager.GetUnLockSpecialBuildings();
          this.m_dicBuildingData[2] = CBuildingManager.GetLockedSpecialBuildings();

          this.m_btnSwitchPage0.SetText( 0, "普通建筑(" + this.GetBuildingsDataByPageIndex(0).length.toString() + ")");
          this.m_btnSwitchPage1.SetText( 0, "已解锁(" +this.GetBuildingsDataByPageIndex(1).length.toString() + ")");
          this.m_btnSwitchPage2.SetText( 0,  "未解锁(" +this.GetBuildingsDataByPageIndex(2).length.toString() + ")");

         this.m_txtCurProcessingLotType.text = "当前选中的地块属性：" + CBuildingManager.PropertyType2String( Main.s_CurTown.GetCurEditProcessingLot().GetLotProperty() );


          this.SwitchPage(0);

      }
      
      ////  解锁或使用
      protected onButtonClick_UnlockOrUse( evt:egret.TouchEvent ):void
      {
          if ( this.m_nCurPageIndex == 0 || this.m_nCurPageIndex == 1 ) // 使用
          {
              CBuildingManager.UseBuilding( this.m_CurBuildingData );
              if ( this.m_CurBuildingData.bSpecial )
              {
                  this.m_CurBuildingData.bUsed = true;
                  this.SelectBuilding();
              }   
          }
          else // 解锁
          {
              CBuildingManager.UnlockBuilding( this.m_CurBuildingData );
          }
      }
     
      public SelectLastBulding():void
      {
          this.SetCurBuildingIndex( this.GetMaxBuildingIndex() );
      }



} // end class