class CUIBaseProgressBar extends eui.Component {

     protected m_imgFilledBar:eui.Image;
     protected m_imgFilledBar_Bg:eui.Image;
     protected m_imgBG:eui.Image;
     protected m_labelTitle:eui.Label;

     protected m_BarTotalWidth:number = 0;
     protected m_nTotalTime:number = 0;
     protected m_fTimeElapse:number = 0;
     protected m_bProcessing:boolean = false;

     protected m_pbMain:CUICosmosProgressBar;

     protected m_shapeMask:egret.Shape = new egret.Shape();

     protected m_nBarTotalWidth:number = 0;
     protected m_nBarTotalHeight:number = 0;
     protected m_nBarPosX:number = 0;
     protected m_nBarPosY:number = 0;

     public constructor( szExmlSource:string ) {
        
        super(  );

        this.SetExml( "resource/assets/MyExml/CProgressBar.exml" );
         this.m_labelTitle = this.getChildByName( "labelTitle" ) as eui.Label;
      
         var imgBar:eui.Image = this.getChildByName( "pbBar" ) as eui.Image;
         imgBar.mask = this.m_shapeMask;
        imgBar.anchorOffsetX = 0;
        imgBar.anchorOffsetY = 0;
         this.m_nBarPosX = imgBar.x;
         this.m_nBarPosY = imgBar.y;
          this.addChild( this.m_shapeMask );
          this.m_nBarTotalWidth = imgBar.width;
          this.m_nBarTotalHeight = imgBar.height;
        /*
        this.m_imgBG = this.getChildByName( "imgBg" ) as eui.Image;
        if ( this.m_imgBG )
        {
            this.m_imgBG.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 211,211,211 ) ];
        }


        this.m_imgFilledBar = this.getChildByName( "imgFilledBar" ) as eui.Image;
        this.m_imgFilledBar.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255(0,255,0) ];
        this.m_BarTotalWidth = this.m_imgFilledBar.width;
        this.m_imgFilledBar.width = 0;

        this.m_imgFilledBar_Bg = this.getChildByName( "imgFilledBarBg" ) as eui.Image;
        this.m_imgFilledBar_Bg.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 0,100,0 ) ];

       
        */
     } // end constructor

     public SetTitle( szTitle:string ):void
     {
         this.m_labelTitle.text = szTitle;
     }

     public SetExml( source:string ):void
     {
         this.skinName = source;
     } 

     public Begin( nTotalTime:number ):void
     {
         this.m_nTotalTime = nTotalTime;
         this.m_fTimeElapse = 0;
         this.m_bProcessing = true;
     }

     public Loop( fPercent:number ):void
     {
       


      if ( fPercent > 1 )
         {
             return;
         }
         //this.m_imgFilledBar.width = this.m_BarTotalWidth * fPercent;
         //this.m_pbMain.SetPercent( fPercent );
         this.m_shapeMask.graphics.clear();
        this.m_shapeMask.graphics.beginFill( 0xFF0000 );
        this.m_shapeMask.graphics.drawRect( this.m_nBarPosX, this.m_nBarPosY, this.m_nBarTotalWidth * fPercent, this.m_nBarTotalHeight );
         
         console.log( this.m_BarTotalWidth );
          this.m_shapeMask.graphics.endFill();
         
     }

     public SetFilledBarColor( r:number, g:number, b:number ):void
     {
        //this.m_imgFilledBar.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b) ];
     }

     public SetFilledBarBgColor(r:number, g:number, b:number):void
     {
        //this.m_imgFilledBar_Bg.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b) ];
     }

     public SetFilledBarWidth( nWidth:number ):void
     {
         //this.m_imgFilledBar.width = nWidth;
     }

     public SetFilledBarTotalWidth(nWidth:number):void
     {
         /*
         this.m_BarTotalWidth = nWidth;

         this.m_imgFilledBar_Bg.width = nWidth;
         */
     }

     public SetFilledBarTotalHeight(nHeight:number):void
     {
         /*
         this.m_imgFilledBar_Bg.height = nHeight;
         this.m_imgFilledBar.height = nHeight;
         */
     }

     public SetPercent( nCurValue:number, nTotalValue:number ):void
     {
         /*
         var fPercent:number = nCurValue / nTotalValue;
         this.m_imgFilledBar.width = this.m_BarTotalWidth * fPercent;
         */
     }

     public SetPercentByPercent( fPercent:number ):void
     {
         this.m_pbMain.SetPercent( fPercent );
       // this.m_imgFilledBar.width = this.m_BarTotalWidth * fPercent;
     }

} // end class