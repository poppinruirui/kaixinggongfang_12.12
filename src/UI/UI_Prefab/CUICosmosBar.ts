class CUICosmosBar extends eui.Component {

     protected m_LeftCircle:egret.Shape = new egret.Shape();
     protected m_RightCircle:egret.Shape = new egret.Shape();
     protected m_MiddleRect:egret.Shape = new egret.Shape();

     public constructor() {
        super();

        this.addChild( this.m_LeftCircle );
        this.addChild( this.m_RightCircle );
        this.addChild( this.m_MiddleRect );

     } // end constructor

     public SetParams( width:number, height:number, color:number, alpha:number = 1 ):void{
         var radius:number = 0.5 * height;

         this.m_LeftCircle.graphics.clear();
        this.m_LeftCircle.graphics.beginFill( color, alpha );
        this.m_LeftCircle.graphics.drawArc(0,0, radius , Math.PI / 2, 1.5 * Math.PI, false);
        this.m_LeftCircle.graphics.endFill();
        this.m_LeftCircle.x = radius;

        this.m_MiddleRect.graphics.clear();
        var middle_width:number = width - height;
        this.m_MiddleRect.graphics.beginFill(color,  alpha);
        this.m_MiddleRect.graphics.drawRect(0,-radius,middle_width, height);
        this.m_MiddleRect.graphics.endFill();
        this.m_MiddleRect.x = radius;
        
        this.m_RightCircle.graphics.clear();
        this.m_RightCircle.graphics.beginFill( color, alpha );
        this.m_RightCircle.graphics.drawArc(0,0, radius , 1.5 * Math.PI, 0.5 * Math.PI, false);
        this.m_RightCircle.graphics.endFill();
        this.m_RightCircle.x = radius + middle_width;
     }

} // end class