class CUIBaseCommonButton extends egret.DisplayObjectContainer {

      protected m_uiBg1:CUIBaseDoubleRectBg;
      protected m_uiBg2:CUIBaseDoubleRectBg;

      protected m_txtTitle:eui.Label;

      public constructor() {
        super();

        var nOutWidth:number = 400;
        var nOutHeight:number = 80;

        this.m_uiBg1 = new CUIBaseDoubleRectBg();
        this.addChild( this.m_uiBg1 );
        this.m_uiBg1.SetType( Global.eDoubleRectBgType.up_down );
        this.m_uiBg1.SetPartSizePercent( 0, 1, 0.9 );
        this.m_uiBg1.SetPartColor( 0, 1,1,1,1 );
        this.m_uiBg1.SetPartColor( 1, 0.663,0.663,0.663,1 );
        this.m_uiBg1.SetSize( nOutWidth, nOutHeight );

         this.m_uiBg2 = new CUIBaseDoubleRectBg();
        this.addChild( this.m_uiBg2 );
        this.m_uiBg2.SetType( Global.eDoubleRectBgType.up_down );
        this.m_uiBg2.SetPartSizePercent( 0, 1, 0.1 );
        this.m_uiBg2.SetPartColor( 0, 0.753,0.2,0.145,1 );
        this.m_uiBg2.SetPartColor( 1, 0.922,0.312,0.227,1 );
        this.m_uiBg2.SetSize( 380, 50 );
        this.m_uiBg2.x = 10;
        this.m_uiBg2.y = 10;

        this.m_txtTitle = new eui.Label();
        this.addChild( this.m_txtTitle );
        this.m_txtTitle.textAlign = egret.HorizontalAlign.CENTER;
        this.m_txtTitle.verticalAlign = egret.VerticalAlign.MIDDLE;
        this.m_txtTitle.text = "关闭";
        this.m_txtTitle.width = nOutWidth;
        this.m_txtTitle.height = nOutHeight * 0.9;
        this.m_txtTitle.size = 25;
      } // end constructor

      
} // end class