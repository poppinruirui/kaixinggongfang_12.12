class CUIBaseCommonButton2 extends egret.DisplayObjectContainer {

      protected m_uiBg1:CUIBaseDoubleRectBg;
      protected m_uiIcon:eui.Image;

      protected m_txtTitle:eui.Label;
      protected m_txtCost:eui.Label;

      public constructor() {
        super();

        var nOutWidth:number = 150;
        var nOutHeight:number = 90;

        this.m_uiBg1 = new CUIBaseDoubleRectBg();
        this.addChild( this.m_uiBg1 );
        this.m_uiBg1.SetType( Global.eDoubleRectBgType.up_down );
        this.m_uiBg1.SetPartSizePercent( 0, 1, 0.9 );
        this.m_uiBg1.SetPartColor( 0, 0.337,0.737,0.212,1 );
        this.m_uiBg1.SetPartColor( 1, 0.216,0.498,0.133,1 );
        this.m_uiBg1.SetSize( nOutWidth, nOutHeight );

  
        this.m_txtTitle = new eui.Label();
        this.addChild( this.m_txtTitle );
         this.m_txtTitle.text = "解锁";
        this.m_txtTitle.y = 15;
        this.m_txtTitle.x = 60;
        this.m_txtTitle.size = 20;

        this.m_txtCost = new eui.Label();
        this.addChild( this.m_txtCost );
         this.m_txtCost.text = "~";
        this.m_txtCost.x = 50;
        this.m_txtCost.y = 52;
        this.m_txtCost.size = 22;
        

        this.m_uiIcon = new eui.Image( CJinBiManager.GetFrameTexture(0) );
        this.addChild(this.m_uiIcon);
        this.m_uiIcon.scaleX = 0.25;
        this.m_uiIcon.scaleY = 0.22;
        this.m_uiIcon.x = 24;
        this.m_uiIcon.y = 52;




      } // end constructor

      public SetCost( nCost:number ):void
      {
        this.m_txtCost.text = nCost.toString();
      }

} // end class