/*
    “帧动画”模块     
*/
class CFrameAni extends egret.DisplayObjectContainer {

       protected m_bmpFrame:egret.Bitmap = new egret.Bitmap();

       protected m_szResKey:string = "";
       protected m_nFrameTotalNum:number = 0;
       protected m_nFrameInterval:number = 0;
       protected m_bLoop:boolean = false;

       protected m_nCurIndex:number = 0;
       protected m_bPlaying:boolean = false;

       protected m_nTimeElapse:number = 0;

       protected m_bByWebUrl:boolean = false;


       public constructor() {
        super();

        this.addChild( this.m_bmpFrame );

       } // end constructor

       public SetParams( res_key:string, total_frames:number, frame_interval:number, loop:boolean = false, bByWebUrl:boolean = false ):void
       {
           this.m_szResKey = res_key;
           this.m_nFrameTotalNum = total_frames;
           this.m_nFrameInterval = frame_interval;
           this.m_bLoop = loop;
           this.m_bByWebUrl = bByWebUrl;
       }

       public ChangeFrame_ByUrl( nIndex:number ):void
       {
         
            var szRes:string = this.m_szResKey + nIndex + ".png";

             RES.getResByUrl( szRes, this.onLoadResComplete, this, RES.ResourceItem.TYPE_IMAGE);

       }

         protected onLoadResComplete(event:any):void {
      
            

        var tex: egret.Texture = <egret.Texture>event;
        this.m_bmpFrame.texture = tex;

          this.anchorOffsetX = this.m_bmpFrame.width / 2;
                this.anchorOffsetY = this.m_bmpFrame.height;
     }

       public ChangeFrame( nIndex:number ):void
       {
            if ( this.m_bByWebUrl )
            {
                this.ChangeFrame_ByUrl( nIndex );
                return;
            }

           var szRes:string = this.m_szResKey + nIndex + "_png";
           this.m_bmpFrame.texture = RES.getRes(szRes);
           this.m_bmpFrame.anchorOffsetX = this.m_bmpFrame.width * 0.5;
           this.m_bmpFrame.anchorOffsetY = this.m_bmpFrame.height * 0.5;
           this.m_bmpFrame.scaleX = 1.2;
           this.m_bmpFrame.scaleY = 1.2;
       }

       public Play( ):void
       {
           this.m_nTimeElapse = 0;
           this.m_bPlaying = true;
           this.ChangeFrame(0);
           this.m_nCurIndex = 0;
       }

       public FixedUpdate():boolean
       {
           if ( !this.m_bPlaying )
           {
               return true;
           }
  
           this.m_nTimeElapse += CDef.s_fFixedDeltaTime;
           if ( this.m_nTimeElapse < this.m_nFrameInterval )
           {
               return true;
           }
           this.m_nTimeElapse = 0;

         ;
           this.m_nCurIndex++;
           if ( this.m_nCurIndex >= this.m_nFrameTotalNum )
           {
               if ( this.m_bLoop )
               {
                this.m_nCurIndex = 0;
               }
              else
              {
                   CFrameAniManager.DeleteEffect( this );
                   return true;
              }
           }

           this.ChangeFrame( this.m_nCurIndex );



           return false;

       }

} // end class