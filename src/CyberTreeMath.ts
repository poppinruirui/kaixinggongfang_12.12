class CyberTreeMath extends CObj {

// 已知位移和时间求初速度（末速度为零）
    public static GetV0( s:number, t:number ):number
    {
        return (2.0 * s / t);
    }

    // 已知位移和时间求加速度（末速度为零）
    public static GetA( s:number,  t:number):number
    {
        return -2.0 * s / (t * t);
    }


    public static LeftOfLine( x:number, y:number, x1:number, y1:number, x2:number, y2):number
    {

        var tmpx:number = (x1 - x2) / (y1 - y2) * (y - y2) + x2;

        if (tmpx >x)//当tmpx>p.x的时候，说明点在线的左边，小于在右边，等于则在线上。
        {
            return 1;
        }
        else if ( tmpx < x )
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
    
    public static ANGLE_2_RADIAN:number = 0.01746;
    public static Angle2Radion( fAngle:number ):number
    {
        return fAngle * CyberTreeMath.ANGLE_2_RADIAN;
    }

    public static Sin( fAngle:number ):number
    {
        return Math.sin( CyberTreeMath.Angle2Radion( fAngle ) );
    }

     public static Cos(fAngle:number ):number
    {   
         return Math.cos( CyberTreeMath.Angle2Radion( fAngle ) );
    }

} // end class