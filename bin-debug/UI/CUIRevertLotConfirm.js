var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIRevertLotConfirm = (function (_super) {
    __extends(CUIRevertLotConfirm, _super);
    function CUIRevertLotConfirm() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CBulldozeLotConfirm.exml";
        _this.addChild(_this.m_uiContainer);
        var imgBtnCancel = _this.m_uiContainer.getChildByName("btnCancelReert");
        imgBtnCancel.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Cancel, _this);
        var imgBtnconfirm = _this.m_uiContainer.getChildByName("btnConfirmRevert");
        imgBtnconfirm.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Confirm, _this);
        /*
    this.m_btnCancel = new CCosmosSimpleButton();
    this.m_uiContainer.addChild( this.m_btnCancel );
    var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img2" ) as eui.Image;
    this.m_btnCancel.x = imgTemp.x;
    this.m_btnCancel.y = imgTemp.y;
    this.m_btnCancel.scaleX = imgTemp.scaleX;
    this.m_btnCancel.scaleY = imgTemp.scaleY;
    this.m_btnCancel.SetImgColor( 0, 121, 121, 121 );
    this.m_btnCancel.SetTextColor( 0 , 0xFFFFFF );
    this.m_btnCancel.SetTextContent( 0 , "取消" );
    this.m_uiContainer.removeChild( imgTemp );
    this.m_btnCancel.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Cancel, this);

    this.m_btnConfirm = new CCosmosSimpleButton();
    this.m_uiContainer.addChild( this.m_btnConfirm );
    var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img3" ) as eui.Image;
    this.m_btnConfirm.x = imgTemp.x;
    this.m_btnConfirm.y = imgTemp.y;
    this.m_btnConfirm.scaleX = imgTemp.scaleX;
    this.m_btnConfirm.scaleY = imgTemp.scaleY;
    this.m_btnConfirm.SetImgColor( 0, 0, 0, 0  );
    this.m_btnConfirm.SetTextColor( 0 , 0xFFFFFF );
    this.m_btnConfirm.SetTextContent( 0 , "确定" );
    this.m_uiContainer.removeChild( imgTemp );
    this.m_btnConfirm.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Confirm, this);
*/
        var img0 = _this.m_uiContainer.getChildByName("img0");
        img0.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(0, 0, 0)];
        img0.alpha = 0.6;
        return _this;
    } // constrcuctor
    CUIRevertLotConfirm.prototype.onButtonClick_Cancel = function (evt) {
        this.visible = false;
    };
    CUIRevertLotConfirm.prototype.onButtonClick_Confirm = function (evt) {
        this.visible = false;
        CUIManager.SetUiVisible(Global.eUiId.lot_upgrade, false);
        Main.s_CurTown.RevertLot();
    };
    return CUIRevertLotConfirm;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIRevertLotConfirm.prototype, "CUIRevertLotConfirm");
//# sourceMappingURL=CUIRevertLotConfirm.js.map