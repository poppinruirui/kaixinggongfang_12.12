var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIDistrict = (function (_super) {
    __extends(CUIDistrict, _super);
    function CUIDistrict() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_nDistrictNum = 0;
        _this.m_containerSketch = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/Distrixt.exml";
        _this.addChild(_this.m_uiContainer);
        _this.addChild(_this.m_containerSketch);
        var imgTemp = _this.m_uiContainer.getChildByName("imgBg");
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(181, 181, 181)];
        var imgTemp = _this.m_uiContainer.getChildByName("btnClose");
        _this.m_btnClose = new CCosmosImage();
        _this.m_btnClose.SetExml("resource/assets/MyExml/CCosmosButton1.exml");
        _this.m_uiContainer.addChild(_this.m_btnClose);
        _this.m_btnClose.x = imgTemp.x;
        _this.m_btnClose.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnClose.UseColorSolution(0);
        _this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Close, _this);
        return _this;
    } // end constructor
    CUIDistrict.prototype.onButtonClick_Close = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.district, false);
    };
    CUIDistrict.prototype.UpdateDistrictInfo = function (nDistrictNum, nCurDistrictId) {
        var bReGenerate = false;
        if (this.m_nDistrictNum != nDistrictNum) {
            bReGenerate = true;
        }
        this.m_nDistrictNum = nDistrictNum;
        if (bReGenerate) {
            this.ReGenerateDistrict();
        }
    };
    CUIDistrict.prototype.ReGenerateDistrict = function () {
        this.Clear();
        var nCenterPosX = 250;
        var nCenterPosY = 300;
        for (var i = 1; i <= this.m_nDistrictNum; i++) {
            var sketch = CResourceManager.NewDistrictSketch();
            sketch.SetDistrictId(i);
            this.m_containerSketch.addChild(sketch);
            // 添加事件之前一定要检查该物件上是不是已经有该事件了。因为整套体系中各种物件都是复用的。
            if (!sketch.hasEventListener(egret.TouchEvent.TOUCH_TAP)) {
                sketch.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onClickSketch, this);
            }
            switch (i) {
                case 1:
                    {
                        sketch.x = 250;
                        sketch.y = 300;
                    }
                    break;
                case 2:
                    {
                        sketch.x = nCenterPosX;
                        sketch.y = nCenterPosY - CDef.s_nTileHeight * 2;
                    }
                    break;
                case 3:
                    {
                        sketch.x = nCenterPosX + CDef.s_nTileWidth;
                        sketch.y = nCenterPosY - CDef.s_nTileHeight;
                    }
                    break;
                case 4:
                    {
                        sketch.x = nCenterPosX + CDef.s_nTileWidth;
                        sketch.y = nCenterPosY;
                    }
                    break;
                case 5:
                    {
                        sketch.x = nCenterPosX + CDef.s_nTileWidth;
                        sketch.y = nCenterPosY + CDef.s_nTileHeight;
                    }
                    break;
                case 6:
                    {
                        sketch.x = nCenterPosX;
                        sketch.y = nCenterPosY + CDef.s_nTileHeight * 2;
                    }
                    break;
                case 7:
                    {
                        sketch.x = nCenterPosX - CDef.s_nTileWidth;
                        sketch.y = nCenterPosY + CDef.s_nTileHeight;
                    }
                    break;
                case 8:
                    {
                        sketch.x = nCenterPosX - CDef.s_nTileWidth;
                        sketch.y = nCenterPosY;
                    }
                    break;
                case 9:
                    {
                        sketch.x = nCenterPosX - CDef.s_nTileWidth * 0.5;
                        sketch.y = nCenterPosY - CDef.s_nTileHeight * 0.5;
                    }
                    break;
            } // end switch
        } // end for
    };
    CUIDistrict.prototype.Clear = function () {
        for (var i = this.m_containerSketch.numChildren - 1; i >= 0; i--) {
            var sketch = this.m_containerSketch.getChildAt(i);
            CResourceManager.DeleteDistrictSketch(sketch);
        }
    };
    CUIDistrict.prototype.onClickSketch = function (evt) {
        var sketch = evt.currentTarget;
        Main.s_CurTown.ChangeDistrict(sketch.GetDistrictId());
    };
    return CUIDistrict;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIDistrict.prototype, "CUIDistrict");
//# sourceMappingURL=CUIDistrict.js.map