var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var UIFlyingCoin = (function (_super) {
    __extends(UIFlyingCoin, _super);
    function UIFlyingCoin() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_imgMain = null;
        _this.m_fSession0_AX = 0;
        _this.m_fSession0_AY = 0;
        _this.m_fSession1_AX = 0;
        _this.m_fSession1_AY = 0;
        _this.m_fMiddlePosX = 0;
        _this.m_fMiddlePosY = 0;
        _this.m_fEndPosX = 0;
        _this.m_fEndPosY = 0;
        _this.m_aryAX = new Array();
        _this.m_aryAY = new Array();
        _this.m_aryStartPosX = new Array();
        _this.m_aryStartPosY = new Array();
        _this.m_aryEndPosX = new Array();
        _this.m_aryEndPosY = new Array();
        _this.m_bFlying = false;
        _this.m_nStatus = 0;
        _this.m_VX = 0;
        _this.m_VY = 0;
        _this.m_uiContainer.skinName = "resource/assets/MyExml/FlyingCoin.exml";
        _this.addChild(_this.m_uiContainer);
        _this.m_imgMain = _this.m_uiContainer.getChildByName("imgMain");
        return _this;
    } // end constructor
    UIFlyingCoin.prototype.SetMiddlePos = function (fMiddlePosX, fMiddlePosY) {
        this.m_fMiddlePosX = fMiddlePosX;
        this.m_fMiddlePosY = fMiddlePosY;
    };
    UIFlyingCoin.prototype.SetEndPos = function (fEndPosX, fEndPosY) {
        this.m_fEndPosX = fEndPosX;
        this.m_fEndPosY = fEndPosY;
    };
    UIFlyingCoin.prototype.AddSessionParams = function (fAX, fAY, fStartPosX, fStartPosY, fEndPosX, fEndPosY) {
        this.m_aryAX.push(fAX);
        this.m_aryAY.push(fAY);
        this.m_aryStartPosX.push(fStartPosX);
        this.m_aryStartPosY.push(fStartPosY);
        this.m_aryEndPosX.push(fEndPosX);
        this.m_aryEndPosY.push(fEndPosY);
    };
    UIFlyingCoin.prototype.BeginFlying = function () {
        this.m_bFlying = true;
        this.m_nStatus = 0;
        this.x = this.m_aryStartPosX[0];
        this.y = this.m_aryStartPosY[0];
        this.m_VX = 0;
        this.m_VY = 0;
        this.visible = true;
    };
    UIFlyingCoin.prototype.Moving = function () {
        if (!this.m_bFlying) {
            return;
        }
        this.m_VX += this.m_aryAX[this.m_nStatus] * CDef.s_fFixedDeltaTime;
        this.m_VY += this.m_aryAY[this.m_nStatus] * CDef.s_fFixedDeltaTime;
        this.x += this.m_VX * CDef.s_fFixedDeltaTime;
        this.y += this.m_VY * CDef.s_fFixedDeltaTime;
        var bEndX = false;
        var bEndY = false;
        if (this.m_VX > 0) {
            if (this.x >= this.m_aryEndPosX[this.m_nStatus]) {
                bEndX = true;
            }
        }
        else if (this.m_VX < 0) {
            if (this.x <= this.m_aryEndPosX[this.m_nStatus]) {
                bEndX = true;
            }
        }
        else {
            bEndX = true;
        }
        if (this.m_VY > 0) {
            if (this.y >= this.m_aryEndPosY[this.m_nStatus]) {
                bEndY = true;
            }
        }
        else if (this.m_VY < 0) {
            if (this.y <= this.m_aryEndPosY[this.m_nStatus]) {
                bEndY = true;
            }
        }
        else {
            bEndY = true;
        }
        if (bEndX && bEndY) {
            this.m_nStatus++;
            this.m_VX = 0;
            this.m_VY = 0;
            if (this.m_nStatus >= 2) {
                this.EndFly();
            }
        }
    };
    UIFlyingCoin.prototype.EndFly = function () {
        this.m_bFlying = false;
        this.visible = false;
        CSoundManager.PlaySE(Global.eSE.small_coin);
    };
    return UIFlyingCoin;
}(eui.Component)); // end class
__reflect(UIFlyingCoin.prototype, "UIFlyingCoin");
//# sourceMappingURL=UIFlyingCoin.js.map