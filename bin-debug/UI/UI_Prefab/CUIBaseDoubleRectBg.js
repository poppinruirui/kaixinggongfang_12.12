var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIBaseDoubleRectBg = (function (_super) {
    __extends(CUIBaseDoubleRectBg, _super);
    function CUIBaseDoubleRectBg() {
        var _this = _super.call(this) || this;
        _this.m_nWidth = 0;
        _this.m_nHeight = 0;
        _this.m_aryParts = new Array();
        _this.m_aryPartsWidthPercent = new Array();
        _this.m_aryPartsHeightPercent = new Array();
        _this.m_eType = Global.eDoubleRectBgType.up_down;
        for (var i = 0; i < 2; i++) {
            _this.m_aryParts[i] = new eui.Image(RES.getRes("1X1_png"));
            _this.addChild(_this.m_aryParts[i]);
        }
        return _this;
    }
    CUIBaseDoubleRectBg.prototype.SetType = function (eType) {
        this.m_eType = eType;
    };
    CUIBaseDoubleRectBg.prototype.SetPartSizePercent = function (nPartIndex, fWidthPercent, fHeightPercent) {
        this.m_aryPartsWidthPercent[nPartIndex] = fWidthPercent;
        this.m_aryPartsHeightPercent[nPartIndex] = fHeightPercent;
    };
    CUIBaseDoubleRectBg.prototype.SetPartColor = function (nPartIndex, r, g, b, a) {
        this.m_aryParts[nPartIndex].filters = [CColorFucker.GetColorMatrixFilterByRGBA(r, g, b, a)];
    };
    CUIBaseDoubleRectBg.prototype.SetSize = function (nWidth, nHeight) {
        this.m_nWidth = nWidth;
        this.m_nHeight = nHeight;
        this.Update();
    };
    CUIBaseDoubleRectBg.prototype.Update = function () {
        if (this.m_eType == Global.eDoubleRectBgType.up_down) {
            this.m_aryParts[0].width = this.m_nWidth;
            this.m_aryParts[0].height = this.m_nHeight * this.m_aryPartsHeightPercent[0];
            this.m_aryParts[1].width = this.m_nWidth;
            this.m_aryParts[1].height = this.m_nHeight - this.m_aryParts[0].height;
            this.m_aryParts[1].y = this.m_aryParts[0].height;
        }
    };
    return CUIBaseDoubleRectBg;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIBaseDoubleRectBg.prototype, "CUIBaseDoubleRectBg");
//# sourceMappingURL=CUIBaseDoubleRectBg.js.map