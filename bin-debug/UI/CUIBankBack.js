var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIBankBack = (function (_super) {
    __extends(CUIBankBack, _super);
    function CUIBankBack() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CBankBack.exml";
        _this.addChild(_this.m_uiContainer);
        var imgTemp = _this.m_uiContainer.getChildByName("img0");
        _this.m_btnCollect2X = new CCosmosImage();
        _this.m_btnCollect2X.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        _this.m_uiContainer.addChild(_this.m_btnCollect2X);
        _this.m_btnCollect2X.x = imgTemp.x;
        _this.m_btnCollect2X.y = imgTemp.y;
        _this.m_btnCollect2X.scaleX = imgTemp.scaleX;
        _this.m_btnCollect2X.scaleY = imgTemp.scaleY;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnCollect2X.SetImageColor_New(0, 105, 105, 105);
        _this.m_btnCollect2X.SetTextColor(0, 0xFFFFFF);
        _this.m_btnCollect2X.SetText(0, "二倍收益(看广告)");
        _this.m_btnCollect2X.SetTextSize(0, 22);
        imgTemp = _this.m_uiContainer.getChildByName("img1");
        _this.m_btnCollect = new CCosmosImage();
        _this.m_btnCollect.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        _this.m_uiContainer.addChild(_this.m_btnCollect);
        _this.m_btnCollect.x = imgTemp.x;
        _this.m_btnCollect.y = imgTemp.y;
        _this.m_btnCollect.scaleX = imgTemp.scaleX;
        _this.m_btnCollect.scaleY = imgTemp.scaleY;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnCollect.SetImageColor_New(0, 28, 28, 28);
        _this.m_btnCollect.SetTextColor(0, 0xFFFFFF);
        _this.m_btnCollect.SetText(0, "收钱");
        _this.m_btnCollect.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Collect, _this);
        //  this.m_btnCollect.SetTextSize( 0, 22 );
        _this.m_txtBankSaving = _this.m_uiContainer.getChildByName("txt0");
        return _this;
    } // end constructor
    CUIBankBack.prototype.onButtonClick_Collect = function (evt) {
        Main.s_CurTown.CollectBankSaving();
        this.visible = false;
    };
    CUIBankBack.prototype.SetBankSaving = function (nBackSaving) {
        this.m_txtBankSaving.text = nBackSaving.toString();
    };
    return CUIBankBack;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIBankBack.prototype, "CUIBankBack");
//# sourceMappingURL=CUIBankBack.js.map