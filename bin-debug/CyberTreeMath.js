var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CyberTreeMath = (function (_super) {
    __extends(CyberTreeMath, _super);
    function CyberTreeMath() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // 已知位移和时间求初速度（末速度为零）
    CyberTreeMath.GetV0 = function (s, t) {
        return (2.0 * s / t);
    };
    // 已知位移和时间求加速度（末速度为零）
    CyberTreeMath.GetA = function (s, t) {
        return -2.0 * s / (t * t);
    };
    CyberTreeMath.LeftOfLine = function (x, y, x1, y1, x2, y2) {
        var tmpx = (x1 - x2) / (y1 - y2) * (y - y2) + x2;
        if (tmpx > x) {
            return 1;
        }
        else if (tmpx < x) {
            return -1;
        }
        else {
            return 0;
        }
    };
    CyberTreeMath.Angle2Radion = function (fAngle) {
        return fAngle * CyberTreeMath.ANGLE_2_RADIAN;
    };
    CyberTreeMath.Sin = function (fAngle) {
        return Math.sin(CyberTreeMath.Angle2Radion(fAngle));
    };
    CyberTreeMath.Cos = function (fAngle) {
        return Math.cos(CyberTreeMath.Angle2Radion(fAngle));
    };
    CyberTreeMath.ANGLE_2_RADIAN = 0.01746;
    return CyberTreeMath;
}(CObj)); // end class
__reflect(CyberTreeMath.prototype, "CyberTreeMath");
//# sourceMappingURL=CyberTreeMath.js.map