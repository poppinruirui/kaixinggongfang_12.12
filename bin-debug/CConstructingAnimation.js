var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
// “正在建造”动画效果
var CConstructingAnimation = (function (_super) {
    __extends(CConstructingAnimation, _super);
    function CConstructingAnimation() {
        var _this = _super.call(this) || this;
        _this.m_bmpGround = new egret.Bitmap(RES.getRes("dibiao-baise_png")); // 底座
        _this.m_containerSegs = new egret.DisplayObjectContainer(); // 按高度动态分配的段数
        _this.m_containerCrane = new egret.DisplayObjectContainer(); // 塔吊（帧动画）
        _this.m_fCraneFrameAniTimeElapse = 0;
        _this.m_nCraneAniFrameIndex = 0;
        _this.m_aryCraneFrams = new Array();
        _this.m_bmpCrane = new egret.Bitmap();
        _this.m_fParentObjWidth = 0;
        _this.m_fParentObjHeight = 0;
        _this.m_fParentObjSizeType = 0;
        _this.m_nCurSegNum = 0;
        //    this.addChild( this.m_bmpGround );
        _this.addChild(_this.m_containerSegs);
        _this.addChild(_this.m_bmpCrane);
        // 滤镜 test
        var colorMatrix = [
            0.411, 0, 0, 0, 0,
            0, 0.411, 0, 0, 0,
            0, 0, 0.411, 0, 0,
            1, 1, 1, 0, 0
        ];
        var colorFlilter = new egret.ColorMatrixFilter(colorMatrix);
        _this.m_bmpGround.filters = [colorFlilter];
        // end 滤镜 test
        var bmpCrane = new egret.Bitmap(RES.getRes("qizhongji1_png"));
        bmpCrane.anchorOffsetX = bmpCrane.width / 2;
        bmpCrane.anchorOffsetY = bmpCrane.height;
        _this.m_aryCraneFrams.push(bmpCrane);
        bmpCrane = new egret.Bitmap(RES.getRes("qizhongji2_png"));
        bmpCrane.anchorOffsetX = bmpCrane.width / 2;
        bmpCrane.anchorOffsetY = bmpCrane.height;
        _this.m_aryCraneFrams.push(bmpCrane);
        bmpCrane = new egret.Bitmap(RES.getRes("qizhongji3_png"));
        bmpCrane.anchorOffsetX = bmpCrane.width / 2;
        bmpCrane.anchorOffsetY = bmpCrane.height;
        _this.m_aryCraneFrams.push(bmpCrane);
        _this.m_bmpCrane.scaleX = 0.5;
        _this.m_bmpCrane.scaleY = 0.5;
        return _this;
    }
    CConstructingAnimation.prototype.SetSegNum = function (nSegNum) {
        var HurdleTotalHeight = 0;
        this.m_nCurSegNum = nSegNum;
        var nNeedNum = nSegNum - this.m_containerSegs.numChildren;
        if (this.m_containerSegs.numChildren < nSegNum) {
            for (var i = 0; i < nNeedNum; i++) {
                var seg = new egret.Bitmap();
                this.m_containerSegs.addChild(seg);
                seg.anchorOffsetX = seg.width * 0.5;
                seg.anchorOffsetY = seg.height;
            }
        }
        var nCount = 0;
        for (var i = this.m_containerSegs.numChildren - 1; i >= 0; i--) {
            var seg = this.m_containerSegs.getChildAt(i);
            if (nCount >= nSegNum) {
                seg.visible = false;
                continue;
            }
            seg.visible = true;
            if (nCount == 0) {
                seg.texture = CResourceManager.s_bmpSegTop.texture;
            }
            else {
                seg.texture = CResourceManager.s_bmpSeg.texture;
            }
            var fSegHeight = -(nSegNum - nCount - 1) * seg.height * 0.17;
            seg.y = fSegHeight;
            HurdleTotalHeight += fSegHeight;
            nCount++;
        }
        if (this.m_fParentObjSizeType == Global.eObjSize.size_2x2) {
            if (nSegNum == 1) {
                this.m_bmpCrane.y = -seg.height * 0.25;
            }
            else {
                this.m_bmpCrane.y = -seg.height * 0.2 - (nSegNum - 1) * seg.height * 0.225;
            }
            this.m_containerSegs.x = -this.m_fParentObjWidth * 0.215;
            this.m_containerSegs.y = -this.m_fParentObjHeight * 0.7;
        }
        else if (this.m_fParentObjSizeType == Global.eObjSize.size_4x4) {
            if (nSegNum == 1) {
                this.m_bmpCrane.y = -seg.height * 0.2;
            }
            else {
                this.m_bmpCrane.y = -seg.height * 0.2 - (nSegNum - 1) * seg.height * 0.225;
            }
            this.m_containerSegs.x = -this.m_fParentObjWidth * 0.1075;
            this.m_containerSegs.y = -this.m_fParentObjHeight * 0.35;
        }
    };
    CConstructingAnimation.prototype.SetParentObjPixelSize = function (eSizeType, fWidth, fHeight) {
        this.m_fParentObjSizeType = eSizeType;
        this.m_fParentObjWidth = fWidth;
        this.m_fParentObjHeight = fHeight;
    };
    CConstructingAnimation.prototype.SetSize = function (eSize) {
        switch (eSize) {
            case Global.eObjSize.size_1x1:
                {
                    this.scaleX = 0.5;
                    this.scaleY = 0.5;
                }
                break;
            case Global.eObjSize.size_2x2:
                {
                    this.scaleX = 1;
                    this.scaleY = 1;
                }
                break;
            case Global.eObjSize.size_4x4:
                {
                    this.scaleX = 2;
                    this.scaleY = 2;
                }
                break;
        } // end switch
    };
    CConstructingAnimation.prototype.FixedUpdate = function () {
        this.CraneFrameAniLoop();
    };
    CConstructingAnimation.prototype.CraneFrameAniLoop = function () {
        this.m_fCraneFrameAniTimeElapse += CDef.s_fBuildingLandLoopInterval;
        if (this.m_fCraneFrameAniTimeElapse < CDef.s_fCraneFrameInterval) {
            return;
        }
        this.m_fCraneFrameAniTimeElapse = 0;
        this.m_nCraneAniFrameIndex = Math.floor(Math.random() * 3);
        if (this.m_nCraneAniFrameIndex >= this.m_aryCraneFrams.length) {
            this.m_nCraneAniFrameIndex = 0;
        }
        var bmpFrame = this.m_aryCraneFrams[this.m_nCraneAniFrameIndex];
        this.m_bmpCrane.texture = bmpFrame.texture;
        this.m_bmpCrane.anchorOffsetX = this.m_bmpCrane.width * 0.5;
        this.m_bmpCrane.anchorOffsetY = this.m_bmpCrane.height;
    };
    CConstructingAnimation.prototype.MainLoop = function () {
        this.CraneFrameAniLoop();
    };
    return CConstructingAnimation;
}(egret.DisplayObjectContainer)); // end class 
__reflect(CConstructingAnimation.prototype, "CConstructingAnimation");
//# sourceMappingURL=CConstructingAnimation.js.map