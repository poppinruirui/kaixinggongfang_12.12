var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CColorFucker = (function (_super) {
    __extends(CColorFucker, _super);
    function CColorFucker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CColorFucker.Init = function () {
        CColorFucker.s_BlurDisabledGray = CColorFucker.GetColorMatrixFilterByRGBA_255(190, 190, 190);
    };
    CColorFucker.SetImageColor = function (img, r, g, b) {
        img.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b)];
    };
    CColorFucker.GetColorMatrixFilterByRGBA_255 = function (r, g, b) {
        r /= 255;
        g /= 255;
        b /= 255;
        return CColorFucker.GetColorMatrixFilterByRGBA(r, g, b);
    };
    CColorFucker.GetColorMatrixFilterByRGBA = function (r, g, b, a) {
        if (a === void 0) { a = 1; }
        var szKey = r + "," + g + "," + "b" + "," + a;
        var colorFlilter = CColorFucker.s_dicColorMatrixFilterByRGBA[szKey];
        if (colorFlilter == undefined) {
            var colorMatrix = [
                r, 0, 0, 0, 0,
                0, g, 0, 0, 0,
                0, 0, b, 0, 0,
                a, a, a, 0, 0
            ];
            colorFlilter = new egret.ColorMatrixFilter(colorMatrix);
            CColorFucker.s_dicColorMatrixFilterByRGBA[szKey] = colorFlilter;
        }
        else {
        }
        return colorFlilter;
    };
    CColorFucker.GetBlurFilter = function () {
        return CColorFucker.s_BlurFilter;
    };
    CColorFucker.GetDisabledGray = function () {
        return CColorFucker.s_BlurDisabledGray;
    };
    // 根据传入的RGBA值，生成颜色滤镜
    // 白鹭引擎暂无内置的着色功能，只有用滤镜来实现着色效果
    CColorFucker.s_dicColorMatrixFilterByRGBA = new Object();
    CColorFucker.s_BlurFilter = new egret.BlurFilter(20, 20);
    CColorFucker.s_BlurDisabledGray = null;
    return CColorFucker;
}(egret.DisplayObjectContainer)); // end class
__reflect(CColorFucker.prototype, "CColorFucker");
//# sourceMappingURL=CColorFucker.js.map